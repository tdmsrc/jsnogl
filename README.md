# JS graphing utility

A simple utility to look at graphs of functions of 2 variables or level sets/density plots of functions of 3 variables.

To use it, go to <http://tdm.bz/jsnogl>.

![](http://tdm.bz/img/nogl/level1-thumb.jpg)
![](http://tdm.bz/img/nogl/level2-thumb.jpg)
![](http://tdm.bz/img/nogl/density-thumb.jpg)
