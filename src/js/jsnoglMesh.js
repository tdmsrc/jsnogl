// REQUIRES: 
// - jsnoglMath.js
// - jsnoglRasterizer.js

//========================================================
// MESH
// Instructions:
// - TODO
// - *NB*: Required to call initializeTransform() at least once before
//   any of cameraTransform, prepareLighting, renderFace
//========================================================
//Mesh vertex properties:
// - position: world coordinates [without object transformation applied]
//
// - positionT: position, *with* object transformation applied
// - eye: eye coordinates output by camera.worldToEye
// - clip: clip coordinates output by camera.eyeToClip
// - index: reserved for generating edges
// - faces: faces touching vertex [createCrossReferences]
//
//Mesh edge properties:
// - p, q: mesh vertices specifying endpoints of edge
//
// - faces: faces touching edge [createCrossReferences]
//
//Mesh face properties: [assumed faces are convex]
// - vertices: array of face-vertices (see Mesh face-vertex)
// - texture: what texture to use for the face (any object with a setAsColor(target, u, v) property)
// - twosided: option for whether or not to cull if back-facing
//
// - normal: for flat shading [without transformation; createFaceNormals]
// - normalT: normal, *with* object transformation applied
// - zsort: reserved for sorting to draw back-to-front
// - li: reserved for flat shading light intensity
// - backfacing: during cameraTransform, determines if poly will be backfacing
// - edges: edges on face [createCrossReferences]
// - mesh: mesh to which the face belongs [createCrossReferences]
//
//Mesh face-vertex properties:
// - p: mesh vertex
// - uvw: texture coordinates
//
// - normal: for gouraud and phong shading [without transformation; createVertexNormals]
// - normalT: normal, *with* object transformation applied
// - li: reserved for gouraud and phong shading light intensity

function Mesh(){
  this.vertices = []; //array of objects (see Mesh vertex)
  this.edges = []; //array of edges (see Mesh edge)
  this.faces = []; //array of faces (see Mesh face)
  
  //some rendering options
  this.faceFillStyle = LightModel.GOURAUD;
  this.edgeStyle = Mesh.EDGESTYLE.PLAIN;
  this.edgeColor = [212,248,212,64];
};

//options for filling and outlining faces
Mesh.EDGESTYLE = { NODRAW: 0, PLAIN: 1, OUTLINE: 2 };

//----------------------------------------
// INITIALIZATION
// (typical usage: define mesh vertices and faces, then call all of these)
//----------------------------------------

//create edges from vertex and face data (no need to call this if manually created)
Mesh.prototype.createEdges = function(){

  //create index for each vertex
  var nverts = this.vertices.length;
  for(var i=0, il=nverts; i<il; i++){
    this.vertices[i].index = i;
  }
  
  //add edges for each face
  this.edges = [];
  var indexPairs = []; //to avoid duplicates
    
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      var v0 = f.vertices[j].p, v1 = f.vertices[(j+1)%jl].p;
      
      //get unique number for index pair
      if(v0.index > v1.index){ var temp = v0; v0 = v1; v1 = temp; } 
      var hash = v0.index*nverts + v1.index;
      
      //add edge if it doesn't already exist
      if(indexPairs[hash] == true){ continue; }
      this.edges.push({p:v0, q:v1}); 
      indexPairs[hash] = true;
    }
  }
};

//must be called after vertices, faces, and edges are created
//(or just vertices and faces, + createEdges)
Mesh.prototype.createCrossReferences = function(){

  //mesh reference for each face
  for(var i=0, il=this.faces.length; i<il; i++){
    this.faces[i].mesh = this;
  }
  
  //list of faces for each mesh vertex
  for(var i=0, il=this.vertices.length; i<il; i++){
    this.vertices[i].faces = [];
  }
  
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      f.vertices[j].p.faces.push(f);
    }
  }
  
  //list of faces for each edge
  for(var i=0, il=this.edges.length; i<il; i++){
    var e = this.edges[i];
    
    e.faces = []; //intersect e.p.faces and e.q.faces
    for(var j=0, jl=e.p.faces.length; j<jl; j++){ var f = e.p.faces[j];
    for(var k=0, kl=e.q.faces.length; k<kl; k++){
      if(e.q.faces[k] == f){ e.faces.push(f); }
    }}
  }
  
  //list of edges for each face
  for(var i=0, il=this.faces.length; i<il; i++){
    this.faces[i].edges = [];
  }
  
  for(var i=0, il=this.edges.length; i<il; i++){
    var e = this.edges[i];
    
    for(var j=0, jl=e.faces.length; j<jl; j++){
      e.faces[j].edges.push(e);
    }
  }
};

//create face normals (if created manually, no need to call this)
Mesh.prototype.createFaceNormals = function(){

  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    var p0 = f.vertices[0].p, p1 = f.vertices[1].p, p2 = f.vertices[2].p;
    var e1 = Vector3d.copy(p1.position); Vector3d.subtract(e1, p0.position);
    var e2 = Vector3d.copy(p2.position); Vector3d.subtract(e2, p0.position);
    f.normal = [0,0,0]; Vector3d.setAsCross(f.normal, e1, e2);
    Vector3d.normalize(f.normal);
  }
};

//create face-vertex normals (if created manually, no need to call)
//if used, must be called after createCrossReferences
Mesh.prototype.createVertexNormals = function(){
  
  //for each face-vertex, average the normals of the faces meeting the vertex
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      var fv = f.vertices[j];
      
      fv.normal = [0,0,0];
      for(var k=0, kl=fv.p.faces.length; k<kl; k++){
        var fn = fv.p.faces[k].normal;
        Vector3d.add(fv.normal, fn);
      }
      Vector3d.normalize(fv.normal);
    }
  }
};

//initialize vertex positionT, face normalT, and face-vertex normalT properties
//should be called after face normals and face-vertex normals have been created
Mesh.prototype.initializeTransform = function(){

  for(var i=0, il=this.vertices.length; i<il; i++){
    var p = this.vertices[i];
    p.positionT = Vector3d.copy(p.position);
    p.eye = [0,0,0];
    p.clip = [0,0,0];
  }
  
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    f.normalT = Vector3d.copy(f.normal);
    
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      var fv = f.vertices[j];
      fv.normalT = Vector3d.copy(fv.normal);
    }
  }
};


//----------------------------------------
// TRANSFORMATION/PREPARATION
//----------------------------------------

//use this to transform a mesh (i.e. independently of camera projection etc)
Mesh.prototype.transform = function(offset, rotation){
  
  for(var i=0, il=this.vertices.length; i<il; i++){
    var p = this.vertices[i];
    
    Matrix3d.setAsTransform(p.positionT, rotation, p.position);
    Vector3d.add(p.positionT, offset);
  }
  
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    Matrix3d.setAsTransform(f.normalT, rotation, f.normal);
    
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      var fv = f.vertices[j];
      Matrix3d.setAsTransform(fv.normalT, rotation, fv.normal);
    }
  }
};

//compute eye and clip coordinates for each vertex, and determine face.backfacing property
Mesh.prototype.cameraTransform = function(camera){
  
  for(var i=0, il=this.vertices.length; i<il; i++){
    var p = this.vertices[i];
    
    camera.setAsWorldToEye(p.eye, p.positionT);
    camera.setAsEyeToClip(p.clip, p.eye);
  }
  
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
      
    var p0c = f.vertices[0].p.clip, p1c = f.vertices[1].p.clip, p2c = f.vertices[2].p.clip;    
    var crossz = (p1c[0]-p0c[0]) * (p2c[1]-p0c[1]) - (p1c[1]-p0c[1]) * (p2c[0]-p0c[0]);
    f.backfacing = (crossz < 0);
  }
};

//determine light intensity at faces or vertices, if necessary
Mesh.prototype.prepareLighting = function(light){
  
  if(!this.faceFillStyle.hasPrepare){ return; }
  
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    this.faceFillStyle.prepare(f, light);
  }
};

//sort faces by average clip z
Mesh.zsortf = function(a, b){ return a.zsort-b.zsort; }; //front to back (i.e., with zbuffer)
//Mesh.zsortf = function(a, b){ return b.zsort-a.zsort; }; //back to front (i.e., no zbuffer)
Mesh.zsort = function(faces){
  
  for(var i=0, il=faces.length; i<il; i++){
    var f = faces[i];
    
    f.zsort = 0;
    for(var j=0, jl=f.vertices.length; j<jl; j++){
      f.zsort += f.vertices[j].p.clip[2];
    }
    f.zsort = f.zsort / f.vertices.length;
  }
  
  faces.sort(Mesh.zsortf);
};

//returns true iff edge has != 2 incident faces, or has 2 incident faces where one is backfacing and one isn't
Mesh.edgeIsOutline = function(e){
  if(e.faces.length != 2){ return true; }

  var hasB = false, hasF = false;
  
  for(var j=0, jl=e.faces.length; j<jl; j++){
    if(e.faces[j].backfacing){ hasB = true; }
    else{ hasF = true; }
  }

  return (hasB && hasF);
};


//----------------------------------------
// RENDERING
//----------------------------------------

Mesh.prototype.renderEdges = function(rasterizer, camera, pattern){
  this.cameraTransform(camera);
  
  for(var i=0, il=this.edges.length; i<il; i++){
    var e = this.edges[i];
    
    switch(this.edgeStyle){
    case Mesh.EDGESTYLE.NODRAW: break;
    case Mesh.EDGESTYLE.PLAIN: rasterizer.line(e.p, e.q, this.edgeColor, pattern); break;
    case Mesh.EDGESTYLE.OUTLINE: if(Mesh.edgeIsOutline(e)){ rasterizer.line(e.p, e.q, this.edgeColor, pattern); } break;
    }
  }
};

Mesh.prototype.render = function(rasterizer, camera, light){
  this.cameraTransform(camera);
  this.prepareLighting(light);
  
  Mesh.zsort(this.faces);
  
  //draw each face
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    Mesh.renderFace(rasterizer, camera, light, f);
  }
};

//assumes cameraTransform and prepareLighting have been called, the latter with matching fillStyle
Mesh.renderFace = function(rasterizer, camera, light, f){
  if(f.backfacing && !f.twosided){ return; }
  
  var faceFillStyle = f.mesh.faceFillStyle;
  
  //triangles
  for(var j=0, jl=f.vertices.length-2; j<jl; j++){
    var fv0 = f.vertices[0], fv1 = f.vertices[j+1], fv2 = f.vertices[j+2];
    
    if(!faceFillStyle.hasFrag){ continue; }
    var frag = faceFillStyle.createFrag(f, fv0, fv1, fv2, light, camera.position);
    
    rasterizer.triangle(fv0.p, fv1.p, fv2.p, frag);
  }
};

//=================================================
// MESH GROUP
// renders meshes with correct z-sorting across multiple meshes
//-------------------------------------------------
// how to use:
// create a MeshGroup and add meshes, then call render (same signature as mesh.render)
//=================================================

function MeshGroup(){
  this.clear();
};

MeshGroup.prototype.clear = function(){
  this.meshes = [];
  this.faces = [];
};

MeshGroup.prototype.add = function(mesh){
  this.meshes.push(mesh);
  this.faces = this.faces.concat(mesh.faces);
};

MeshGroup.prototype.render = function(rasterizer, camera, light){
  
  for(var i=0, il=this.meshes.length; i<il; i++){
    var m = this.meshes[i];
    
    m.cameraTransform(camera);
    m.prepareLighting(light);
  }
  
  Mesh.zsort(this.faces);
  
  //draw each face
  for(var i=0, il=this.faces.length; i<il; i++){
    var f = this.faces[i];
    
    Mesh.renderFace(rasterizer, camera, light, f);
  }
};

MeshGroup.prototype.renderEdges = function(rasterizer, camera){

  for(var i=0, il=this.meshes.length; i<il; i++){
    var mesh = this.meshes[i];
    
    mesh.renderEdges(rasterizer, camera);
  }
};


//=================================================
// LIGHTING MODELS (for mesh.faceFillStyle)
//-------------------------------------------------
// how to use: 
// boolean properties hasPrepare and hasFrag
// if hasPrepare, then call prepare(f, light) during prepareLighting
// if hasFrag, then call createFrag(f, fv0, fv1, fv2, light, eyepos)
// to produce a "frag shader" for use with Rasterizer.triangle
//-------------------------------------------------
// a light is an object with the following properties:
// lightdir, lightpos, ambcoeff, diffcoeff, speccoeff, specexp
//-------------------------------------------------
// eyepos = world coordinates of eye (i.e., camera.position)
//=================================================

function LightModel(){ };

//NO DRAW
//-----------------------------------------------------
LightModel.NODRAW = { hasPrepare: false, hasFrag: false };

//NO LIGHT
//-----------------------------------------------------
LightModel.NOLIGHT = {
  hasPrepare: false, hasFrag: true,
  
  createFrag: function(f, fv0, fv1, fv2, light, eyepos){
    return function(pColor, l0, l1, l2){
      var u = fv0.uvw[0]*l0 + fv1.uvw[0]*l1 + fv2.uvw[0]*l2,
          v = fv0.uvw[1]*l0 + fv1.uvw[1]*l1 + fv2.uvw[1]*l2,
          w = fv0.uvw[2]*l0 + fv1.uvw[2]*l1 + fv2.uvw[2]*l2;
      f.texture.setAsColor(pColor, u, v, w);
    };
  }
};

//FLAT: compute light intensity for each face, no interpolation
//-----------------------------------------------------
LightModel.FLAT = {
  hasPrepare: true, hasFrag: true,
  
  prepare: function(f, light){
    if(f.backfacing && !f.twosided){ return; }
  
    //2 sided lighting
    f.li = -light.diffcoeff * Vector3d.dot(f.normalT, light.dir);
    if(f.backfacing){ f.li = -f.li; } if(f.li < 0){ f.li = 0; }
    f.li += light.ambcoeff; //note f.li could be > 1
  },
  
  createFrag: function(f, fv0, fv1, fv2, light, eyepos){
    return function(pColor, l0, l1, l2){
      var u = fv0.uvw[0]*l0 + fv1.uvw[0]*l1 + fv2.uvw[0]*l2,
          v = fv0.uvw[1]*l0 + fv1.uvw[1]*l1 + fv2.uvw[1]*l2,
          w = fv0.uvw[2]*l0 + fv1.uvw[2]*l1 + fv2.uvw[2]*l2;
      f.texture.setAsColor(pColor, u, v, w);
      
      pColor[0] *= f.li;  if(pColor[0] > 255){ pColor[0] = 255; }
      pColor[1] *= f.li;  if(pColor[1] > 255){ pColor[1] = 255; }
      pColor[2] *= f.li;  if(pColor[2] > 255){ pColor[2] = 255; }
    };
  }
};

//GOURAUD: compute light intensity for each face-vertex, interpolate for pixels
//-----------------------------------------------------
LightModel.GOURAUD = {
  hasPrepare: true, hasFrag: true,
  
  prepare: function(f, light){
    if(f.backfacing && !f.twosided){ return; }
      
    for(j=0, jl=f.vertices.length; j<jl; j++){
      var fv = f.vertices[j];
    
      //2 sided lighting
      fv.li = -light.diffcoeff * Vector3d.dot(fv.normalT, light.dir); 
      if(f.backfacing){ fv.li = -fv.li; } if(fv.li < 0){ fv.li = 0; }
      fv.li += light.ambcoeff; //note fv.li could be > 1
    }
  },
  
  createFrag: function(f, fv0, fv1, fv2, light, eyepos){
    return function(pColor, l0, l1, l2){
      var u = fv0.uvw[0]*l0 + fv1.uvw[0]*l1 + fv2.uvw[0]*l2,
          v = fv0.uvw[1]*l0 + fv1.uvw[1]*l1 + fv2.uvw[1]*l2,
          w = fv0.uvw[2]*l0 + fv1.uvw[2]*l1 + fv2.uvw[2]*l2;
      f.texture.setAsColor(pColor, u, v, w);
  
      var li = fv0.li*l0 + fv1.li*l1 + fv2.li*l2;
      pColor[0] *= li;  if(pColor[0] > 255){ pColor[0] = 255; }
      pColor[1] *= li;  if(pColor[1] > 255){ pColor[1] = 255; }
      pColor[2] *= li;  if(pColor[2] > 255){ pColor[2] = 255; }
    };
  }
};

//CEL: like gouraud but discretize light intensity
//-----------------------------------------------------
LightModel.CEL = {
  hasPrepare: true, hasFrag: true,
  
  prepare: LightModel.GOURAUD.prepare,
  
  createFrag: function(f, fv0, fv1, fv2, light, eyepos){
    return function(pColor, l0, l1, l2){
      var u = fv0.uvw[0]*l0 + fv1.uvw[0]*l1 + fv2.uvw[0]*l2,
          v = fv0.uvw[1]*l0 + fv1.uvw[1]*l1 + fv2.uvw[1]*l2,
          w = fv0.uvw[2]*l0 + fv1.uvw[2]*l1 + fv2.uvw[2]*l2;
      f.texture.setAsColor(pColor, u, v, w);
      
      var li = fv0.li*l0 + fv1.li*l1 + fv2.li*l2;
      
      var outA = 0.7, outB = 1.0, outC = 1.2; //light intensities in dark, medium, and bright regions
      var inAB = 0.5, inBC = 0.9; //original light intensities bounding dark, medium, and bright regions
      var bw = 0.01; //governs transition between regions; smaller = sharper, bigger = smoother
      
      var tli = 0;
      if(li < inAB-bw){ tli = outA; }
      else if(li < inAB+bw){ tli = outA + (outB-outA)*(li-inAB+bw)/(2*bw); }
      else if(li < inBC-bw){ tli = outB; }
      else if(li < inBC+bw){ tli = outB + (outC-outB)*(li-inBC+bw)/(2*bw); }
      else{ tli = outC; }
      
      pColor[0] *= tli;  if(pColor[0] > 255){ pColor[0] = 255; }
      pColor[1] *= tli;  if(pColor[1] > 255){ pColor[1] = 255; }
      pColor[2] *= tli;  if(pColor[2] > 255){ pColor[2] = 255; }
    };
  }
};

//PHONG: compute light intensity per pixel
//-----------------------------------------------------
//creates: fragpos, n, ftl, fte, blinnh

LightModel.PHONG = {
  hasPrepare: false, hasFrag: true,
  
  createFrag: function(f, fv0, fv1, fv2, light, eyepos){
    return function(pColor, l0, l1, l2){
      var u = fv0.uvw[0]*l0 + fv1.uvw[0]*l1 + fv2.uvw[0]*l2,
          v = fv0.uvw[1]*l0 + fv1.uvw[1]*l1 + fv2.uvw[1]*l2,
          w = fv0.uvw[2]*l0 + fv1.uvw[2]*l1 + fv2.uvw[2]*l2;
      f.texture.setAsColor(pColor, u, v, w);
      
      //frag position
      var fragpos = [
        fv0.p.positionT[0]*l0 + fv1.p.positionT[0]*l1 + fv2.p.positionT[0]*l2,
        fv0.p.positionT[1]*l0 + fv1.p.positionT[1]*l1 + fv2.p.positionT[1]*l2,
        fv0.p.positionT[2]*l0 + fv1.p.positionT[2]*l1 + fv2.p.positionT[2]*l2
      ];
      
      //frag normal
      var n = [
        fv0.normalT[0]*l0 + fv1.normalT[0]*l1 + fv2.normalT[0]*l2,
        fv0.normalT[1]*l0 + fv1.normalT[1]*l1 + fv2.normalT[1]*l2,
        fv0.normalT[2]*l0 + fv1.normalT[2]*l1 + fv2.normalT[2]*l2
      ];
      if(f.backfacing){ Vector3d.scale(n, -1); }
      Vector3d.normalize(n);
      
      //frag to light vector
      var ftl = Vector3d.copy(light.pos); Vector3d.subtract(ftl, fragpos);
      var dftl = Math.sqrt(Vector3d.dot(ftl,ftl));
      Vector3d.scale(ftl, 1/dftl);
      
      //diffuse intensity (and flip normal if necessary for 2 sided lighting)
      var lidiff = light.diffcoeff * Vector3d.dot(n, ftl); 
      if(lidiff < 0){ lidiff = 0; }
      
      //specular intensity
      var fte = Vector3d.copy(eyepos); Vector3d.subtract(fte, fragpos); Vector3d.normalize(fte);
      var blinnh = Vector3d.copy(ftl); Vector3d.add(blinnh, fte); Vector3d.normalize(blinnh);
      var lispec0 = Vector3d.dot(n, fte); if(lispec0 < 0){ lispec0 = 0; }
      var lispec = light.speccoeff * Math.pow(lispec0, light.specexp);
      
      //combine to final color
      pColor[0] = (lidiff + light.ambcoeff)*pColor[0] + lispec*255;
      pColor[1] = (lidiff + light.ambcoeff)*pColor[1] + lispec*255;
      pColor[2] = (lidiff + light.ambcoeff)*pColor[2] + lispec*255;
      
      if(pColor[0] < 0){ pColor[0] = 0; }else if(pColor[0] > 255){ pColor[0] = 255; }
      if(pColor[1] < 0){ pColor[1] = 0; }else if(pColor[1] > 255){ pColor[1] = 255; }
      if(pColor[2] < 0){ pColor[2] = 0; }else if(pColor[2] > 255){ pColor[2] = 255; }
    };
  }
};

