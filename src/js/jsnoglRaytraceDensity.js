// REQUIRES: 
// - jsnoglMath.js
// - jsnoglRasterizer.js

function DensityRaytracer(){ };

//========================================================
// Utility functions
//========================================================

//return p+t(q-p)
//--------------------------------------------------------
DensityRaytracer.lerp3 = function(p, q, t){
  var x = Vector3d.copy(q); 
  Vector3d.subtract(x, p);
  Vector3d.scale(x, t); 
  Vector3d.add(x, p);
  return x;
};

//clip the segment pq between the planes x_i = range[0] and x_i = range[1]
//returns false if none of the edge is left
//--------------------------------------------------------
DensityRaytracer.clipCoord = function(i, p, q, range){
  
  //check which side of range[0] each point is on
  var pLMin = (p[i] < range[0]), qLMin = (q[i] < range[0]);
  
  //if both have x_i < range[0], quit
  if(pLMin && qLMin){ return false; }
  
  //if points on different sides of x_i = range[0], clip
  if(pLMin != qLMin){
  	var t = (range[0] - p[i]) / (q[i] - p[i]);  
    var x = DensityRaytracer.lerp3(p, q, t);
	  if(pLMin){ Vector3d.setEqualTo(p,x); }else{ Vector3d.setEqualTo(q,x); }
  }
  
  //check which side of range[1] each point is on
  var pGMax = (p[i] > range[1]), qGMax = (q[i] > range[1]);

  //if both have x_i > range[1], quit
  if(pGMax && qGMax){ return false; }
  
  //if points on different sides of x_i = range[1], clip
  if(pGMax != qGMax){
  	var t = (range[1] - p[i]) / (q[i] - p[i]);  
    var x = DensityRaytracer.lerp3(p, q, t);
	  if(pGMax){ Vector3d.setEqualTo(p,x); }else{ Vector3d.setEqualTo(q,x); }
  }
  
  //indicate a clip occurred
  return true;
};

//clip the segment pq to the box with x range xr = [xmin,xmax], etc
//returns false if pq is totally outside the box, otherwise true
//--------------------------------------------------------
DensityRaytracer.clipBox = function(p, q, xr, yr, zr){
  return ( 
    DensityRaytracer.clipCoord(0,p,q,xr) && 
    DensityRaytracer.clipCoord(1,p,q,yr) && 
    DensityRaytracer.clipCoord(2,p,q,zr) );
};

//clamp c[i] to [0,255] for i = 0 to 4
//--------------------------------------------------------
DensityRaytracer.clampColor = function(c){
    if(c[0] < 0){ c[0] = 0; }else if(c[0] > 255){ c[0] = 255; }
    if(c[1] < 0){ c[1] = 0; }else if(c[1] > 255){ c[1] = 255; }
    if(c[2] < 0){ c[2] = 0; }else if(c[2] > 255){ c[2] = 255; }
    if(c[3] < 0){ c[3] = 0; }else if(c[3] > 255){ c[3] = 255; }
};


//========================================================
// Models for function value -> alpha value
//========================================================
function AlphaModel(){ };

//cubic smoothing, f:[0,1]->[0,1] with f'(0)=f'(1)=0
AlphaModel.SMOOTH = {
  get: function(fvt){
    return fvt*fvt*(3-2*fvt); 
  }
};

AlphaModel.BANDS = {
  get: function(fvt){
    if((fvt > 0.05) && (fvt < 0.15)){ return 1-Math.abs(0.1-fvt)*20; } else
    if((fvt > 0.25) && (fvt < 0.35)){ return 1-Math.abs(0.3-fvt)*20; } else
    if((fvt > 0.45) && (fvt < 0.55)){ return 1-Math.abs(0.5-fvt)*20; } else
    if((fvt > 0.65) && (fvt < 0.75)){ return 1-Math.abs(0.7-fvt)*20; } else
    if((fvt > 0.85) && (fvt < 0.95)){ return 1-Math.abs(0.9-fvt)*20; }
    return 0;
  }
};


//========================================================
// Raytrace Density Function
//--------------------------------------------------------
//opts: { 
//  alphaModel: AlphaModel.BANDS, nr: 50, //samples along ray
//  inx: [min,max], iny: [min,max], inz: [min,max],
//  outx: [min,max], outy: [min,max], outz: [min,max],
//  f: function(x,y,z){ return number; }
//}
//--------------------------------------------------------
//Usage: call DensityRaytracer.prepare before DensityRaytracer.raytrace
//(no need to call again if changing alphaModel or nr)
//========================================================

//create opts.maxpqlen, opts.fvt
//--------------------------------------------------------
DensityRaytracer.prepare = function(opts){

  //options
  var PERCENT_MIN = 0.15; //fv in smallest PERCENT_MIN will clamp to fvt = 0
  var PERCENT_MAX = 0.15; //fv in greatest PERCENT_MAX will clamp to fvt = 1
  
  //find max diameter of the out box
  //------------------------------------
  var diam = opts.outx[1]-opts.outx[0];  opts.maxpqlen = diam;
  diam = opts.outy[1]-opts.outy[0];  if(diam > opts.maxpqlen){ opts.maxpqlen = diam; }
  diam = opts.outz[1]-opts.outz[0];  if(diam > opts.maxpqlen){ opts.maxpqlen = diam; }
  
  //find min/max of function
  //------------------------------------
  var fmin = 10000, fmax = -10000;
  
  var x,y,z;
  for(var ix=0, ixl=20; ix<=ixl; ix++){ x = opts.inx[0]+(opts.inx[1]-opts.inx[0])*(ix/ixl); 
  for(var iy=0, iyl=20; iy<=iyl; iy++){ y = opts.iny[0]+(opts.iny[1]-opts.iny[0])*(iy/iyl); 
  for(var iz=0, izl=20; iz<=izl; iz++){ z = opts.inz[0]+(opts.inz[1]-opts.inz[0])*(iz/izl); 
    var fv = opts.f(x,y,z);
    if(fv < fmin){ fmin = fv; }
    if(fv > fmax){ fmax = fv; }
  }}}
  
  //adjust for PERCENT_MIN/PERCENT_MAX
  var df = fmax - fmin;
  fmin = fmin + PERCENT_MIN*df;
  fmax = fmax - PERCENT_MAX*df;
  df = fmax - fmin;
  
  //utility functions for evaluating function from out-box coords
  //------------------------------------
  var chainx = (opts.inx[1] - opts.inx[0]) / (opts.outx[1] - opts.outx[0]),
      chainy = (opts.iny[1] - opts.iny[0]) / (opts.outy[1] - opts.outy[0]),
      chainz = (opts.inz[1] - opts.inz[0]) / (opts.outz[1] - opts.outz[0]);
  
  //convert a point in the out-box to in-box coordinates, then get fv as lerp in [fmin,fmax]
  opts.fvt = function(p){
    //evaluate function
    //TODO: converting from out-box to in-box adds ~25ms to render
    var fv = opts.f(
      opts.inx[0] + chainx*(p[0] - opts.outx[0]),
      opts.iny[0] + chainy*(p[1] - opts.outy[0]),
      opts.inz[0] + chainz*(p[2] - opts.outz[0])
    );
    
    //proportional value within [fmin,fmax]
    var t = (fv-fmin)/df;
    //clamp to [0,1]
    return (t < 0) ? 0 : ((t > 1) ? 1 : t);
  };
};

//render to rast
//--------------------------------------------------------
DensityRaytracer.raytrace = function(rast, camera, opts){
  
  //temp variables
  var i=0; //buffer index
  var wy = [0,0,0], w = [0,0,0]; //pixel's world vector
  var color = [0,0,0,0]; //color accumulation along ray
  
  //options
  var ALPHA_SCALE_FACTOR = 2; //multiplies final alpha value
  var CMIN = [0,0,255]; //color for fmin
  var CMAX = [255,0,0]; //color for fmax
  
  for(var iy=0, iyl=rast.height; iy<iyl; iy++){
    //start computing pixel's world vector
    Vector3d.setAsLinearCombination(wy, rast.scrToWorldCoeff1, 1, rast.scrToWorldCoeffy, iy);
    
    for(var ix=0, ixl=rast.width; ix<ixl; ix++){
      //finish computing pixel's world vector
      Vector3d.setAsLinearCombination(w, wy, 1, rast.scrToWorldCoeffx, ix);
      Vector3d.normalize(w);
      
      //clip ray to out-box (pq) and split into nr pieces (dp)
      var p = Vector3d.copy(camera.position);
      var q = Vector3d.copy(w); Vector3d.scale(q,20); Vector3d.add(q,p);
      
      if(!DensityRaytracer.clipBox(p, q, opts.outx, opts.outy, opts.outz)){
        //set alpha to 0
        i+=3; rast.buffer[i++] = 0;
        continue;
      }
      
      var pq = Vector3d.copy(q); Vector3d.subtract(pq,p);
      var pqlen = Math.sqrt(Vector3d.dot(pq,pq));
      
      var dp = Vector3d.copy(pq); Vector3d.scale(dp, 1/opts.nr);
      var dplen = pqlen/opts.nr;
      
      //initial color
      color[0]=0; color[1]=0; color[2]=0; color[3]=0;
      
      var totalweight = 0;
      for(var ir=0; ir<opts.nr; ir++){
        //proportional function value clamped to [0,1]; alpha model value
        var fvt = opts.fvt(p);
        var alpha = opts.alphaModel.get(fvt);
        
        //blend color according to function value, opacity, and ray piece length (dplen)
        var w0 = alpha*dplen*(1-fvt);
        var w1 = alpha*dplen*fvt;
        totalweight += (w0+w1);
        
        color[0] += w0*CMIN[0] + w1*CMAX[0];
        color[1] += w0*CMIN[1] + w1*CMAX[1];
        color[2] += w0*CMIN[2] + w1*CMAX[2];
        color[3] += alpha*dplen;
        
        //move ray forward
        Vector3d.add(p, dp);
      }
      
      //divide by total weights, scale color[3] by 255, and clamp to [0,255]
      Vector3d.scale(color, 1/totalweight); //doesn't affect color[3]
      color[3] = ALPHA_SCALE_FACTOR * 255*color[3] / opts.maxpqlen;
      DensityRaytracer.clampColor(color);
      
      //draw pixel
      if(rast.modeLuminance){
        var l = (0.299*color[0] + 0.587*color[1] + 0.114*color[2]) | 0; //floor
        rast.buffer[i++] = l; 
        rast.buffer[i++] = l;
        rast.buffer[i++] = l;
      }else{
        rast.buffer[i++] = color[0]; 
        rast.buffer[i++] = color[1]; 
        rast.buffer[i++] = color[2]; 
      } 
      rast.buffer[i++] = color[3];
    }
  }
};
