// REQUIRES: 
// - jsnoglMath.js
// - jsnoglMesh.js

//========================================================
// Range
//========================================================
function Interval(){
  this.min = 0;
  this.max = 0;
  this.n = 0;
};

Interval.prototype.width = function(minimum){
  if(typeof(minimum) === 'undefined'){ minimum = -1; }
  
  var w = this.max - this.min;
  return (w < minimum) ? minumum : w;
};

Interval.prototype.fit = function(value){
  if(this.n == 0){
    this.min = value;
    this.max = value;
  }else{
    if(value < this.min){ this.min = value; }
    if(value > this.max){ this.max = value; }
  }
  this.n++;
};


//========================================================
// OBJ loader
//--------------------------------------------------------
// objLines = array of each line in an OBJ file
// opts: { outx: [min,max], outz: [min,max], outy: [min,max] } 
// extra data: mesh.coordBoxOpts
//========================================================
Mesh.obj = function(opts, objLines, tex, twosided){ //TODO, warningCallback, errorCallback){
  
  var v = [], vn = [], vt = [], f_raw = [];
  var has_uv = false;
  
  var xrange = new Interval(), yrange = new Interval(), zrange = new Interval();
  
  //read "v # # #"
  var readV = function(explodedLine){
    if(explodedLine.length < 4){ return; } //TODO error
    if(explodedLine.length > 4){ } //TODO warning
    
    var x = parseFloat(explodedLine[1]); xrange.fit(x); 
    var y = parseFloat(explodedLine[2]); yrange.fit(y);
    var z = parseFloat(explodedLine[3]); zrange.fit(z);
    var rpos = [x,y,z];
    
    //create mesh vertex
    v.push({ position: rpos });
  };
  
  //read "vn # # #"
  var readVN = function(explodedLine){
    if(explodedLine.length < 4){ return; } //TODO error
    if(explodedLine.length > 4){ } //TODO warning
    
    vn.push([ parseFloat(explodedLine[1]), parseFloat(explodedLine[2]), parseFloat(explodedLine[3]) ]);
  };
  
  //read "vt # #"
  var readVT = function(explodedLine){
    if(explodedLine.length < 3){ return; } //TODO error
    if(explodedLine.length > 3){ } //TODO warning
    
    vt.push([ parseFloat(explodedLine[1]), parseFloat(explodedLine[2]), 0 ]);
  };
  
  //read "f v ...", OR "f v/vt ...", OR "f v/vt/vn ...", OR "f v//vn ..."
  var readF = function(explodedLine){
    if(explodedLine.length < 3){ return; } //TODO error
    
    //push an object onto f_raw, which is an array of objects (fv_raw) with properties vi, (vt, vn)
    var raw = [];
    for(var j=1, jl=explodedLine.length; j<jl; j++){
      var data = explodedLine[j].split("/");
      //create the fv_raw
      var vi = parseInt(data[0]),
          vt = (data.length > 1) ? parseInt(data[1]) : -1,
          vn = (data.length > 2) ? parseInt(data[2]) : -1;
      raw.push({ vi: vi, vt: vt, vn: vn });
    }
    f_raw.push(raw);
  };
  
  //read all lines
  for(var i=0, il=objLines.length; i<il; i++){
    var explodedLine = objLines[i].match(/\S+/g);
    if(!explodedLine){ continue; }
    
    switch(explodedLine[0]){
    case "v": readV(explodedLine); break;
    case "vt": readVT(explodedLine); break;
    case "vn": readVN(explodedLine); break;
    case "f": readF(explodedLine); break;
    }
  }
  
  //shift and proportionally scale vertex positions to respect opts.out ranges
  var scalex = (opts.outx[1] - opts.outx[0]) / xrange.width(0.1),
      scaley = (opts.outy[1] - opts.outy[0]) / yrange.width(0.1),
      scalez = (opts.outz[1] - opts.outz[0]) / zrange.width(0.1);
      
  var scalemin = scalex;
  if(scaley < scalemin){ scalemin = scaley; }
  if(scalez < scalemin){ scalemin = scalez; }
  
  var cxout = (opts.outx[1] + opts.outx[0]) * 0.5,
      cyout = (opts.outy[1] + opts.outy[0]) * 0.5,
      czout = (opts.outz[1] + opts.outz[0]) * 0.5;
  var cxin = (xrange.min + xrange.max) * 0.5,
      cyin = (yrange.min + yrange.max) * 0.5,
      czin = (zrange.min + zrange.max) * 0.5;
  
  for(var i=0, il=v.length; i<il; i++){
    var p = v[i];
    p.position = [
      (p.position[0]-cxin)*scalemin + cxout,
      (p.position[1]-cyin)*scalemin + cyout,
      (p.position[2]-czin)*scalemin + czout
    ];
  }
  
  //assemble data
  var f = [];
  for(var i=0, il=f_raw.length; i<il; i++){
    var raw = f_raw[i];
    
    var fv = [];
    for(var j=0, jl=raw.length; j<jl; j++){
      var fvdata = raw[j];
      
      //create face-vertex
      fv.push({ p:v[fvdata.vi-1], uvw:vt[fvdata.vt-1], normal:vn[fvdata.vn-1] });
    }
    f.push({ vertices: fv });
  }
  
  for(var i=0, il=f.length; i<il; i++){
    f[i].texture = tex;
    f[i].twosided = twosided;
  }
  
  //create and return mesh
  var mesh = new Mesh();
  mesh.vertices = v;
  mesh.faces = f;  
  
  mesh.coordBoxOpts = {
    outx: [(xrange.min-cxin)*scalemin+cxout,(xrange.max-cxin)*scalemin+cxout], rangex: [xrange.min,xrange.max], labelx: "x", 
    outy: [(yrange.min-cyin)*scalemin+cyout,(yrange.max-cyin)*scalemin+cyout], rangey: [yrange.min,yrange.max], labely: "y",
    outz: [(zrange.min-czin)*scalemin+czout,(zrange.max-czin)*scalemin+czout], rangez: [zrange.min,zrange.max], labelz: "z"
  };
  
  //initialize and return
  mesh.createEdges();
  mesh.createCrossReferences();
  mesh.createFaceNormals();  
  //mesh.createVertexNormals();
  mesh.initializeTransform();
  return mesh;
};
