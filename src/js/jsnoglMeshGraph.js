// REQUIRES: 
// - jsnoglMath.js
// - jsnoglMesh.js

//========================================================
// Range
//========================================================
function Interval(){
  this.min = 0;
  this.max = 0;
  this.n = 0;
};

Interval.prototype.width = function(minimum){
  if(typeof(minimum) === 'undefined'){ minimum = -1; }
  
  var w = this.max - this.min;
  return (w < minimum) ? minumum : w;
};

Interval.prototype.fit = function(value){
  if(this.n == 0){
    this.min = value;
    this.max = value;
  }else{
    if(value < this.min){ this.min = value; }
    if(value > this.max){ this.max = value; }
  }
  this.n++;
};


//========================================================
// Graph
//--------------------------------------------------------
//opts: { 
//  inx: [min,max], iny: [min,max], nx: #samples, ny: #samples, 
//  outx: [min,max], outz: [min,max], outy: [min,max], 
//  f: function(x,y){} 
//}
//--------------------------------------------------------
//Extra data:
// - Mesh:
//    - fmin, fmax, coordBoxOpts
//    - applyColorFunction(uvw) (uvw = function(x,y,z) evaluates at (fposition, fvalue))
// - Mesh vertex:
//    - fposition (input (x,y) scaled appropriately by opts.inx and opts.iny limits)
//    - fvalue
//========================================================
Mesh.graph = function(opts, tex){
  
  //function scaled so input is [0,1]^2
  var lerpIn = function(tx, ty){
    return [
      (opts.inx[1]-opts.inx[0])*tx + opts.inx[0], 
      (opts.iny[1]-opts.iny[0])*ty + opts.iny[0]
    ];
  };
  
  var frange = new Interval();
  var newverts = [];
  for(var iy=0; iy<opts.ny; iy++){
  for(var ix=0; ix<opts.nx; ix++){
    var tx = ix/(opts.nx-1), ty = iy/(opts.ny-1);
    
    var fpos = lerpIn(tx, ty);
    var fval = opts.f(fpos[0], fpos[1]); 
    frange.fit(fval);
    
    var p = {
      fvalue: fval,
      fposition: fpos,
      position: [
        (opts.outx[1]-opts.outx[0])*tx + opts.outx[0], 0,
        (opts.outz[1]-opts.outz[0])*(1-ty) + opts.outz[0]
      ]
    };
    newverts.push(p);
  }}
  
  //scale y values to fit into outy range
  for(var i=0; i<newverts.length; i++){
    var p = newverts[i];
    var t = (p.fvalue - frange.min)/frange.width(0.1);
    p.position[1] = opts.outy[0] + t*(opts.outy[1] - opts.outy[0]);
  }
  
  var newfaces = [];
  for(var iy=0; iy<opts.ny-1; iy++){
  for(var ix=0; ix<opts.nx-1; ix++){
    
    var p0 = newverts[(iy  )*opts.nx+(ix  )];
    var p1 = newverts[(iy  )*opts.nx+(ix+1)];
    var p2 = newverts[(iy+1)*opts.nx+(ix+1)];
    var p3 = newverts[(iy+1)*opts.nx+(ix  )];
    
    var uv0 = [(ix  )/(opts.nx-1), (iy  )/(opts.ny-1), (p0.fvalue-frange.min)/frange.width(0.1)];
    var uv1 = [(ix+1)/(opts.nx-1), (iy  )/(opts.ny-1), (p1.fvalue-frange.min)/frange.width(0.1)];
    var uv2 = [(ix+1)/(opts.nx-1), (iy+1)/(opts.ny-1), (p2.fvalue-frange.min)/frange.width(0.1)];
    var uv3 = [(ix  )/(opts.nx-1), (iy+1)/(opts.ny-1), (p3.fvalue-frange.min)/frange.width(0.1)];
    
    var fv0 = { p:p0, uvw:uv0 };
    var fv1 = { p:p1, uvw:uv1 };
    var fv2 = { p:p2, uvw:uv2 };
    var fv3 = { p:p3, uvw:uv3 };
    
    //see whether p0-p2 diagonal or p1-p3 diagonal is better fit
    var tx = (ix+0.5)/(opts.nx-1), ty = (iy+0.5)/(opts.ny-1);
    var fmidpos = lerpIn(tx, ty);
    var fmid = opts.f(fmidpos[0], fmidpos[1]);
    
    var mid02 = Math.abs(2*fmid - (p0.fvalue + p2.fvalue));
    var mid13 = Math.abs(2*fmid - (p1.fvalue + p3.fvalue));
    
    if(mid02 < mid13){
      newfaces.push({ vertices: [fv0, fv1, fv2] });
      newfaces.push({ vertices: [fv0, fv2, fv3] });
    }else{
      newfaces.push({ vertices: [fv1, fv2, fv3] });
      newfaces.push({ vertices: [fv1, fv3, fv0] });
    }
  }}
    
  for(var i=0, il=newfaces.length; i<il; i++){
    newfaces[i].texture = tex;
    newfaces[i].twosided = true;
  }
  
  //create mesh and applyColorFunction 
  var mesh = new Mesh();
  mesh.vertices = newverts;
  mesh.faces = newfaces;  
  
  mesh.fmin = frange.min; mesh.fmax = frange.max;
  mesh.coordBoxOpts = {
    outx: opts.outx, rangex: [opts.inx[0],opts.inx[1]], labelx: "x", 
    outy: opts.outy, rangey: [frange.min,frange.max], labely: "z",
    outz: opts.outz, rangez: [opts.iny[1],opts.iny[0]], labelz: "y"
  };
  
  mesh.applyColorFunction = function(uvw){
    
    for(var i=0, il=this.vertices.length; i<il; i++){
      var p = this.vertices[i];
      p.uvw = uvw(p.fposition[0], p.fposition[1], p.fvalue);
    }
    
    for(var i=0, il=this.faces.length; i<il; i++){
      var f = this.faces[i];
    
      for(var j=0, jl=f.vertices.length; j<jl; j++){
        var fv = f.vertices[j];
        fv.uvw = fv.p.uvw;
      }
    }
  };
  
  //initialize and return
  mesh.createEdges();
  mesh.createCrossReferences();
  mesh.createFaceNormals();  
  mesh.createVertexNormals();
  mesh.initializeTransform();
  return mesh;
};

//========================================================
// Graph slices
//========================================================
Mesh.graphSlices = function(graph, nx, ny, nz){
  
  var mesh = new Mesh();
  
  var mix3 = function(p0, p1, t){
    return [ (p1[0]-p0[0])*t+p0[0], (p1[1]-p0[1])*t+p0[1], (p1[2]-p0[2])*t+p0[2] ];
  };
  
  //assumes sliceValue has been set on all graph vertices; create level set sliceValue = c
  var addSlice = function(c){
    //find slice vertices
    for(var i=0, il=graph.edges.length; i<il; i++){
      var e = graph.edges[i];
      
      e.hasSliceVertex = false;
      
      var fp = e.p.sliceValue, fq = e.q.sliceValue;
      if( ((fp < c) && (fq < c)) || ((fp > c) && (fq > c)) ){ continue; }
      var t = (c - fp)/(fq - fp);
      
      e.hasSliceVertex = true;
      e.sliceVertex = { 
        fvalue: c,
        position: mix3(e.p.position, e.q.position, t)
      };
      mesh.vertices.push(e.sliceVertex);
    }
    
    //create edges between slice vertices
    for(var i=0, il=graph.faces.length; i<il; i++){
      var f = graph.faces[i];
      
      //NOTE: assumes the faces are triangles
      var sliceVertices = [];
      for(var j=0, jl=f.edges.length; j<jl; j++){
        var e = f.edges[j];
        if(e.hasSliceVertex){ sliceVertices.push(e.sliceVertex); }
      }
      
      if(sliceVertices.length >= 2){
        mesh.edges.push({p:sliceVertices[0], q:sliceVertices[1]});
      }
    }
    
    //clear the slice vertices stored in edges
    for(var i=0, il=graph.edges.length; i<il; i++){
      var e = graph.edges[i];
      
      e.hasSliceVertex = false;
      e.sliceVertex = null;
    }
  };
  
  //create slice edges
  //---------------------------------
  var slrange;
  
  //x slices
  slrange = new Interval();
  for(var j=0, jl=graph.vertices.length; j<jl; j++){
    var slv = graph.vertices[j].fposition[0]; 
    slrange.fit(slv); 
    graph.vertices[j].sliceValue = slv;
  }
  
  for(var i=1; i<=nx; i++){
    addSlice(slrange.min + (i/(nx+1))*(slrange.max - slrange.min));
  }
  
  //y slices
  slrange = new Interval();
  for(var j=0, jl=graph.vertices.length; j<jl; j++){
    var slv = graph.vertices[j].fposition[1]; 
    slrange.fit(slv); 
    graph.vertices[j].sliceValue = slv;
  }
  
  for(var i=1; i<=ny; i++){
    addSlice(slrange.min + (i/(ny+1))*(slrange.max - slrange.min));
  }
  
  //z slices
  slrange = new Interval();
  for(var j=0, jl=graph.vertices.length; j<jl; j++){
    var slv = graph.vertices[j].fvalue;
    slrange.fit(slv); 
    graph.vertices[j].sliceValue = slv;
  }
  
  for(var i=1; i<=nz; i++){
    addSlice(slrange.min + (i/(nz+1))*(slrange.max - slrange.min));
  }
  //---------------------------------
  
  //initialize and return slice mesh
  mesh.initializeTransform();
  return mesh;
};

