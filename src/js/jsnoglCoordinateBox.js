// REQUIRES: 
// - jsnoglMath.js
// - jsnoglMesh.js
// uses context

//========================================================
// Box
//--------------------------------------------------------
// opts: { outx: [min,max], outz: [min,max], outy: [min,max] } 
// mesh edges have "axis" property, indicating which axis the edge is parallel to (0=x, 1=y, 2=z)
// edges oriented so that for axis=i edges, e.p.position[i] < e.q.position[i]
//========================================================
Mesh.box = function(opts, tex){
  var mesh = new Mesh();
  
  //vertices
  var p0 = { position: [ opts.outx[0], opts.outy[0], opts.outz[0] ] };
  var p1 = { position: [ opts.outx[0], opts.outy[0], opts.outz[1] ] };
  var p2 = { position: [ opts.outx[0], opts.outy[1], opts.outz[0] ] };
  var p3 = { position: [ opts.outx[0], opts.outy[1], opts.outz[1] ] };
  var p4 = { position: [ opts.outx[1], opts.outy[0], opts.outz[0] ] };
  var p5 = { position: [ opts.outx[1], opts.outy[0], opts.outz[1] ] };
  var p6 = { position: [ opts.outx[1], opts.outy[1], opts.outz[0] ] };
  var p7 = { position: [ opts.outx[1], opts.outy[1], opts.outz[1] ] };
  mesh.vertices = [p0,p1,p2,p3,p4,p5,p6,p7];
  
  //faces
  var fx0 = { vertices: [ {p:p0, uv:[0,0]}, {p:p1, uv:[1,0]}, {p:p3, uv:[1,1]}, {p:p2, uv:[0,1]} ] };
  var fx1 = { vertices: [ {p:p5, uv:[0,0]}, {p:p4, uv:[1,0]}, {p:p6, uv:[1,1]}, {p:p7, uv:[0,1]} ] };
  var fy0 = { vertices: [ {p:p0, uv:[0,0]}, {p:p4, uv:[1,0]}, {p:p5, uv:[1,1]}, {p:p1, uv:[0,1]} ] };
  var fy1 = { vertices: [ {p:p3, uv:[0,0]}, {p:p7, uv:[1,0]}, {p:p6, uv:[1,1]}, {p:p2, uv:[0,1]} ] };
  var fz0 = { vertices: [ {p:p4, uv:[0,0]}, {p:p0, uv:[1,0]}, {p:p2, uv:[1,1]}, {p:p6, uv:[0,1]} ] };
  var fz1 = { vertices: [ {p:p1, uv:[0,0]}, {p:p5, uv:[1,0]}, {p:p7, uv:[1,1]}, {p:p3, uv:[0,1]} ] };
  mesh.faces = [ fx0,fx1, fy0,fy1, fz0,fz1 ];
  
  for(var i=0, il=mesh.faces.length; i<il; i++){
    var f = mesh.faces[i];
    f.texture = tex;
    f.twosided = false;
  }
  
  //edges (could use createEdges, but convenient to have the "axis" property)
  mesh.edges = [
    {p:p0, q:p4, axis:0}, {p:p1, q:p5, axis:0}, {p:p2, q:p6, axis:0}, {p:p3, q:p7, axis:0},
    {p:p0, q:p2, axis:1}, {p:p1, q:p3, axis:1}, {p:p4, q:p6, axis:1}, {p:p5, q:p7, axis:1},
    {p:p0, q:p1, axis:2}, {p:p2, q:p3, axis:2}, {p:p4, q:p5, axis:2}, {p:p6, q:p7, axis:2}
  ];
  
  //initialize and return
  //mesh.createEdges();
  mesh.createCrossReferences();
  mesh.createFaceNormals();  
  mesh.createVertexNormals();
  mesh.initializeTransform();
  return mesh;
};

//========================================================
// Coordinate Box
//--------------------------------------------------------
// opts = { outx, rangex, labelx, outy, rangey, labely, outz, rangez, labelz }
//========================================================

function CoordinateBox(opts){
  this.mesh = Mesh.box({ outx: opts.outx, outy: opts.outy, outz: opts.outz });
  this.center = [
    (opts.outx[0] + opts.outx[1]) * 0.5,
    (opts.outy[0] + opts.outy[1]) * 0.5,
    (opts.outz[0] + opts.outz[1]) * 0.5
  ];
  
  this.mesh.faceFillStyle = LightModel.NODRAW;
  this.mesh.edgeStyle = Mesh.EDGESTYLE.PLAIN
  this.mesh.edgeColor = [255,255,255,64];
  
  this.opts = opts;
};

CoordinateBox.prototype.renderBack = function(rast, camera){
  this.mesh.renderEdges(rast, camera, Rasterizer.PATTERN.DASH);
};

CoordinateBox.prototype.renderFront = function(rast, camera){
  this.mesh.render(rast, camera);
};

CoordinateBox.getScr = function(rast, pclip){
  return [ Math.round((1+pclip[0])*0.5*rast.width), Math.round((1-pclip[1])*0.5*rast.height) ];
};

CoordinateBox.prototype.getScrCenter = function(rast, camera){
  
  var ceye = [0,0,0], cclip = [0,0,0];
  camera.setAsWorldToEye(ceye, this.center);
  camera.setAsEyeToClip(cclip, ceye);
  return CoordinateBox.getScr(rast, cclip);
};

CoordinateBox.prototype.renderEdge = function(e, n, rast, camera, context){
    
  //some helper functions
  var mix3 = function(p0, p1, t){
    return [ (p1[0]-p0[0])*t+p0[0], (p1[1]-p0[1])*t+p0[1], (p1[2]-p0[2])*t+p0[2] ];
  };
  
  var elerp = function(t){
    var pclip = [0,0,0];
    camera.setAsEyeToClip(pclip, mix3(e.p.eye, e.q.eye, t));
    return CoordinateBox.getScr(rast, pclip);  
  };
  
  //find outward pointing normal in screen coordinates
  var pscr = CoordinateBox.getScr(rast, e.p.clip), 
      qscr = CoordinateBox.getScr(rast, e.q.clip);
  var nx = pscr[1] - qscr[1], ny = qscr[0] - pscr[0];
  var invlen = 1 / Math.sqrt(nx*nx + ny*ny);
  nx *= invlen; ny *= invlen;
  
  var ec = elerp(0.5);
  var bc = this.getScrCenter(rast, camera);
  var dot = nx*(ec[0] - bc[0]) + ny*(ec[1] - bc[1]);
  if(dot < 0){ nx = -nx; ny = -ny; }
  
  //get label and range
  var label, range0;
  switch(e.axis){
  case 0: label = this.opts.labelx; range0 = this.opts.rangex; break;
  case 1: label = this.opts.labely; range0 = this.opts.rangey; break;
  case 2: label = this.opts.labelz; range0 = this.opts.rangez; break;
  }
  
  //see if range is flipped
  var range = [range0[0], range0[1]], flip = false;
  if(range[0] > range[1]){ 
    range = [range0[1], range0[0]]; flip = true;
  }
  
  //get biggest power of 10 less than range, then set tickstep to closest multiple (1-9) of that to range
  var tickstep0 = (range[1] - range[0])/n; 
  var tickstep1 = Math.pow(10, Math.floor(Math.log(tickstep0) / Math.LN10)); 
  
  var tickstep = tickstep1; 
  var diff = Math.abs(tickstep - tickstep0);
  for(var k=2; k<10; k++){
    if(Math.abs(k*tickstep1 - tickstep0) < diff){ 
      tickstep = k*tickstep1; 
      diff = Math.abs(tickstep - tickstep0); 
    }
  }
  
  //render labels along edge
  var tickOffset = 15, labelOffset = 35;
  
  var i0 = Math.ceil(range[0] / tickstep); //first tick multiple
  for(var i=i0, il=i0+1000; i<il; i++){
    var tickval = Math.round(i*tickstep * 100000) / 100000; //prevent accuracy errors in the tick labels
    if(tickval > range[1]){ break; }
    
    var t = (tickval - range[0])/(range[1] - range[0]); 
    if(flip){ t = 1-t; }
    var tscr = elerp(t);
    
    context.fillText(tickval, tscr[0] + nx*tickOffset, tscr[1] + ny*tickOffset);
  }
  
  var tscr = elerp(0.5);
  context.fillText(label, tscr[0] + nx*labelOffset, tscr[1] + ny*labelOffset);
};

CoordinateBox.prototype.renderLabels = function(rast, camera, context){

  //compute length of edge in screen coords
  var elen = function(e){  
    var pscr = CoordinateBox.getScr(rast, e.p.clip), 
        qscr = CoordinateBox.getScr(rast, e.q.clip);
    var dx = pscr[0] - qscr[0], dy = pscr[1] - qscr[1];
    return Math.sqrt(dx*dx + dy*dy);
  };
  
  //find longest boundary edge parallel to each axis
  var edgex, edgey, edgez;
  var maxlenx=-1, maxleny=-1, maxlenz=-1;
  
  for(var i=0; i<this.mesh.edges.length; i++){
    var e = this.mesh.edges[i];
    if(!Mesh.edgeIsOutline(e)){ continue; }
    
    var len = elen(e);
    switch(e.axis){
    case 0: if(len > maxlenx){ maxlenx = len; edgex = e; } break;
    case 1: if(len > maxleny){ maxleny = len; edgey = e; } break;
    case 2: if(len > maxlenz){ maxlenz = len; edgez = e; } break;
    }
  }  
  
  //number of labels: between 2 and 10, adjust for length
  var targetPixelsPerLabel = 50;
  var nx = (maxlenx / targetPixelsPerLabel); if(nx < 2){ nx = 2; }
  var ny = (maxleny / targetPixelsPerLabel); if(ny < 2){ ny = 2; }
  var nz = (maxlenz / targetPixelsPerLabel); if(nz < 2){ nz = 2; }
    
  //render labels along the edges found
  if(typeof(edgex) !== 'undefined'){ this.renderEdge(edgex, nx, rast, camera, context); }
  if(typeof(edgey) !== 'undefined'){ this.renderEdge(edgey, ny, rast, camera, context); }
  if(typeof(edgez) !== 'undefined'){ this.renderEdge(edgez, nz, rast, camera, context); }
};

