// REQUIRES: 
// - jsnoglMath.js
// - jsnoglMesh.js

//========================================================
// Range
//========================================================
function Interval(){
  this.min = 0;
  this.max = 0;
  this.n = 0;
};

Interval.prototype.width = function(minimum){
  if(typeof(minimum) === 'undefined'){ minimum = -1; }
  
  var w = this.max - this.min;
  return (w < minimum) ? minumum : w;
};

Interval.prototype.fit = function(value){
  if(this.n == 0){
    this.min = value;
    this.max = value;
  }else{
    if(value < this.min){ this.min = value; }
    if(value > this.max){ this.max = value; }
  }
  this.n++;
};


//========================================================
// Contour Data (3D level set)
//--------------------------------------------------------
//opts: { 
//  inx: [min,max], iny: [min,max], inz: [min,max], nx: #samples, ny: #samples, nz: #samples,
//  outx: [min,max], outy: [min,max], outz: [min,max],
//  f: function(x,y){ return number; }
//}
//--------------------------------------------------------
//after using contourData, the grid edges will have mesh vertices stored in them
//these need to be cleared by calling cd.clearEdges (see end of contour and contourSlices)
//extra data: coordBoxOpts
//========================================================

Mesh.contourData = function(opts){
  
  //function scaled appropriately for input and output ranges
  var chainx = (opts.inx[1]-opts.inx[0])/(opts.outx[1]-opts.outx[0]), 
      chainy = (opts.iny[1]-opts.iny[0])/(opts.outy[1]-opts.outy[0]), 
      chainz = (opts.inz[1]-opts.inz[0])/(opts.outz[1]-opts.outz[0]);
      
  var lerpIn = function(tx, ty, tz){
    return [ 
      (opts.inx[1]-opts.inx[0])*tx + opts.inx[0], 
      (opts.iny[1]-opts.iny[0])*ty + opts.iny[0], 
      (opts.inz[1]-opts.inz[0])*tz + opts.inz[0] 
    ];
  };
  var lerpOut = function(tx, ty, tz){
    return [ 
      (opts.outx[1]-opts.outx[0])*tx + opts.outx[0], 
      (opts.outy[1]-opts.outy[0])*ty + opts.outy[0], 
      (opts.outz[1]-opts.outz[0])*tz + opts.outz[0] 
    ];
  };
  
  var ft = function(tx,ty,tz){
    var ev = lerpIn(tx,ty,tz);
    return opts.f(ev[0],ev[1],ev[2]);
  };
  
  //make function value grid:
  //grid[ix][iy][iz] is an object with properties fvalue and position,
  //ix ranges from 0 to nx-1, similarly for iy, iz
  //-----------------------------
  var frange = new Interval();
  
  gv = [];
  for(var ix=0; ix<opts.nx; ix++){ var tx = ix/(opts.nx-1); gv.push([]);
  for(var iy=0; iy<opts.ny; iy++){ var ty = iy/(opts.ny-1); gv[ix].push([]);
  for(var iz=0; iz<opts.nz; iz++){ var tz = iz/(opts.nz-1);
    var fval = ft(tx,ty,tz);
    frange.fit(fval);
    
    gv[ix][iy].push({
      fvalue: fval,
      fposition: lerpIn(tx,ty,tz),
      uvw: [tx, ty, tz],
      position: lerpOut(tx,ty,tz)
    });
  }}}
  
  //approximate gradient using grid function values
  for(var ix=0; ix<opts.nx; ix++){ 
  for(var iy=0; iy<opts.ny; iy++){ 
  for(var iz=0; iz<opts.nz; iz++){
    var gv0,gv1, gvn=[0,0,0];
    
    gv0 = (ix > 0) ? gv[ix-1][iy][iz] : gv[ix][iy][iz];
    gv1 = (ix < (opts.nx-1)) ? gv[ix+1][iy][iz] : gv[ix][iy][iz];
    gvn[0] = chainx * (gv1.fvalue - gv0.fvalue) / (gv1.fposition[0] - gv0.fposition[0]);
    
    gv0 = (iy > 0) ? gv[ix][iy-1][iz] : gv[ix][iy][iz];
    gv1 = (iy < (opts.ny-1)) ? gv[ix][iy+1][iz] : gv[ix][iy][iz];
    gvn[1] = chainy * (gv1.fvalue - gv0.fvalue) / (gv1.fposition[1] - gv0.fposition[1]);
    
    gv0 = (iz > 0) ? gv[ix][iy][iz-1] : gv[ix][iy][iz];
    gv1 = (iz < (opts.nz-1)) ? gv[ix][iy][iz+1] : gv[ix][iy][iz];
    gvn[2] = chainz * (gv1.fvalue - gv0.fvalue) / (gv1.fposition[2] - gv0.fposition[2]);
    
    gv[ix][iy][iz].grad = gvn;
  }}}
  
  //create level set points along grid edges
  //each edge has gv0, gv1, and p (mesh vertex); p stored here to avoid adding to a mesh more than once
  //-----------------------------
  var mix3 = function(p0, p1, t){
    return [ (p1[0]-p0[0])*t+p0[0], (p1[1]-p0[1])*t+p0[1], (p1[2]-p0[2])*t+p0[2] ];
  };
  
  //gv0 and gv1 are objects in grid
  function gridEdgeObject(gv0, gv1){
    this.gv0 = gv0; //grid vertex endpoints
    this.gv1 = gv1;
    this.hasMeshVertex = false;
    
    this.clear = function(){
      this.p = null;
      this.hasMeshVertex = false;
    };
    
    this.getp = function(c, mesh){
      if(this.hasMeshVertex){ return this.p; }
      this.hasMeshVertex = true;
      
      //create and return new mesh vertex along the edge connecting gv0 and gv1
      var f0 = gv0.fvalue, f1 = gv1.fvalue;
      var t = (c - f0)/(f1 - f0);
      this.p = { 
        fposition: mix3(gv0.fposition, gv1.fposition, t),
        position: mix3(gv0.position, gv1.position, t),
        normal: mix3(gv0.grad, gv1.grad, t),
        uvw: mix3(gv0.uvw, gv1.uvw, t)
      };
      Vector3d.normalize(this.p.normal);
      mesh.vertices.push(this.p);
      return this.p;
    };
  };
  
  //ge[ix][iy][iz] is the edge from grid[ix][iy][iz] to grid[ix+dx][iy+dy][iz+dz]
  var geAll = [];
  var gridEdges = function(dx, dy, dz){
    var ge = [];
    for(var ix=0; ix<opts.nx-dx; ix++){ ge.push([]);
    for(var iy=0; iy<opts.ny-dy; iy++){ ge[ix].push([]);
    for(var iz=0; iz<opts.nz-dz; iz++){ 
      var e = new gridEdgeObject(gv[ix][iy][iz], gv[ix+dx][iy+dy][iz+dz]);
      ge[ix][iy].push(e);
      geAll.push(e);
    }}}
    return ge;
  };
  
  return { 
    fmin: frange.min, fmax: frange.max,
    coordBoxOpts: {
      outx: opts.outx, rangex: opts.inx, labelx: "x", 
      outy: opts.outy, rangey: opts.iny, labely: "y",
      outz: opts.outz, rangez: opts.inz, labelz: "z"
    },
    nx: opts.nx, ny: opts.ny, nz: opts.nz,
    geX  :gridEdges(1,0,0), geY :gridEdges(0,1,0), geZ :gridEdges(0,0,1),
    geYZ :gridEdges(0,1,1), geXZ:gridEdges(1,0,1), geXY:gridEdges(1,1,0),
    geXYZ:gridEdges(1,1,1),
    
    clearEdges: function(){ 
      for(var i=0, il=geAll.length; i<il; i++){ geAll[i].clear(); }
    }
  };
};


//========================================================
// Contour (3D level set)
//--------------------------------------------------------
//Extra data:
// - Mesh:
//   - applyColorFunction(uvw) (uvw = function(x,y,z) evaluates at fposition)
// - Mesh vertex:
//   - fposition (input (x,y,z) scaled appropriately by opts.inx, opts.iny, opts.inz limits)
//========================================================

Mesh.contour = function(contourData, c, tex, twosided){
  
  var cd = contourData;
  var mesh = new Mesh();
  
  //create faces from level set vertices
  //-----------------------------
  //tetra is made up of grid vertices 0,1,2,3; argument eij is edge object from grid vertex i to j
  var addTetraFaces = function(e01, e02, e03, e12, e13, e23){
    
    //tetra vertices
    var tv = [ e01.gv0, e01.gv1, e02.gv1, e03.gv1 ];
    
    //triangle along edges incident to tv[i] oriented (flip ? CW : CCW) WRT tv[i]
    var levelTri = function(i, flip){
      
      //get level set vertices along edges incident to tv[i]
      var vp, vq, vr;
      switch(i){
      case 0: vp = e01.getp(c, mesh); vq = e02.getp(c, mesh); vr = e03.getp(c, mesh); break;
      case 1: vp = e01.getp(c, mesh); vq = e12.getp(c, mesh); vr = e13.getp(c, mesh); break;
      case 2: vp = e02.getp(c, mesh); vq = e12.getp(c, mesh); vr = e23.getp(c, mesh); break;
      case 3: vp = e03.getp(c, mesh); vq = e13.getp(c, mesh); vr = e23.getp(c, mesh); break;
      }
      
      //ensure CCW orientation (or CW if flip == true)
      var up = Vector3d.copy(vp.position); Vector3d.subtract(up, tv[i].position);
      var uq = Vector3d.copy(vq.position); Vector3d.subtract(uq, tv[i].position);
      var ur = Vector3d.copy(vr.position); Vector3d.subtract(ur, tv[i].position);
      var upxur = [0,0,0]; Vector3d.setAsCross(upxur, up,ur);
      var ccw = (Vector3d.dot(uq, upxur) > 0);
      if(flip == ccw){ var temp = vp; vp = vr; vr = temp; }
      
      //create the face-vertices and mesh face
      var fvp = { p:vp, uvw:vp.uvw, normal:vp.normal },
          fvq = { p:vq, uvw:vq.uvw, normal:vq.normal },
          fvr = { p:vr, uvw:vr.uvw, normal:vr.normal };
      
      mesh.faces.push({ vertices: [fvp, fvq, fvr] });
    };
    
    //utility function to get the level set vertex along edge eij
    var getEdgeVertex = function(i, j){
      switch(i){
      case 0: switch(j){ case 1: return e01.getp(c, mesh); case 2: return e02.getp(c, mesh); case 3: return e03.getp(c, mesh); }
      case 1: switch(j){ case 0: return e01.getp(c, mesh); case 2: return e12.getp(c, mesh); case 3: return e13.getp(c, mesh); }
      case 2: switch(j){ case 0: return e02.getp(c, mesh); case 1: return e12.getp(c, mesh); case 3: return e23.getp(c, mesh); }
      case 3: switch(j){ case 0: return e03.getp(c, mesh); case 1: return e13.getp(c, mesh); case 2: return e23.getp(c, mesh); }
      }
    };
    
    //quad along the 4 edges incident to edge ij oriented CCW WRT edge ij
    var levelQuad = function(i, j){
    
      //get level set vertices making up the quad, in non-figure-8 order
      var k,l;
      for(k=0; k<=3; k++){ if((k!=i) && (k!=j)){ break; }}
      for(l=3; l>=0; l--){ if((l!=i) && (l!=j)){ break; }}
      var vp = getEdgeVertex(i,k), vq = getEdgeVertex(k,j), vr = getEdgeVertex(j,l), vs = getEdgeVertex(l,i);
      
      //ensure CCW orientation
      var up = Vector3d.copy(vp.position); Vector3d.subtract(up, tv[i].position);
      var uq = Vector3d.copy(vq.position); Vector3d.subtract(uq, tv[i].position);
      var ur = Vector3d.copy(vr.position); Vector3d.subtract(ur, tv[i].position);
      var upxur = [0,0,0]; Vector3d.setAsCross(upxur, up,ur);
      var ccw = (Vector3d.dot(uq, upxur) > 0);
      if(!ccw){ var temp = vp; vp = vr; vr = temp; }
      
      //create the face-vertices and mesh face
      var fvp = { p:vp, uvw:vp.uvw, normal:vp.normal },
          fvq = { p:vq, uvw:vq.uvw, normal:vq.normal },
          fvr = { p:vr, uvw:vr.uvw, normal:vr.normal },
          fvs = { p:vs, uvw:vs.uvw, normal:vs.normal };
      
      mesh.faces.push({ vertices: [fvp, fvq, fvr, fvs] });
    };
    
    //no face, triangle, or quad, depending on how many negative vertices
    var n = 0, ipos = [], ineg = 0;
    for(var i=0; i<4; i++){
      if(tv[i].fvalue < c){ n++; ineg=i; }
      else{ ipos.push(i); }
    }
    
    switch(n){
    case 0: return;
    case 4: return;
    case 1: levelTri(ineg, true); break;
    case 3: levelTri(ipos[0], false); break;
    case 2: levelQuad(ipos[0], ipos[1]); break;
    }
  };
  
  //   3-----7  <- index each grid cube's vertices like this
  //  /|    /|                     
  // / 2---/-6    z+               these tetrahedra      these are obtained from
  // 1-----5 /     | / y+          split the prism with  the first 3 by replacing
  // |/    |/      |/              edges 02, 46, 57      each with its "antipodal"
  // 0-----4       o--- x+                  |                   |
  //                                        v                   v
  //   split cube into tetrahedra: [2670, 6704, 7045], [5107, 1073, 0732]
  
  for(var ix=0; ix<cd.nx-1; ix++){ 
  for(var iy=0; iy<cd.ny-1; iy++){ 
  for(var iz=0; iz<cd.nz-1; iz++){ 
    addTetraFaces( //tetra 2670 > edges 02 06 07 26 27 67
      cd.geY[ix][iy  ][iz], cd.geXY[ix][iy  ][iz], cd.geXYZ[ix  ][iy  ][iz],
      cd.geX[ix][iy+1][iz], cd.geXZ[ix][iy+1][iz], cd.geZ  [ix+1][iy+1][iz]);
    addTetraFaces( //tetra 6704 > edges 04 06 07 46 47 67
      cd.geX[ix  ][iy][iz], cd.geXY[ix  ][iy][iz], cd.geXYZ[ix  ][iy  ][iz],
      cd.geY[ix+1][iy][iz], cd.geYZ[ix+1][iy][iz], cd.geZ  [ix+1][iy+1][iz]);
    addTetraFaces( //tetra 7045 > edges 04 05 07 45 47 57
      cd.geX[ix  ][iy][iz], cd.geXZ[ix  ][iy][iz], cd.geXYZ[ix  ][iy][iz  ],
      cd.geZ[ix+1][iy][iz], cd.geYZ[ix+1][iy][iz], cd.geY  [ix+1][iy][iz+1]);
    addTetraFaces( //tetra 5107 > edges 01 05 07 15 17 57
      cd.geZ[ix][iy][iz  ], cd.geXZ[ix][iy][iz  ], cd.geXYZ[ix  ][iy][iz  ],
      cd.geX[ix][iy][iz+1], cd.geXY[ix][iy][iz+1], cd.geY  [ix+1][iy][iz+1]);
    addTetraFaces( //tetra 1073 > edges 01 03 07 13 17 37
      cd.geZ[ix][iy][iz  ], cd.geYZ[ix][iy][iz  ], cd.geXYZ[ix][iy  ][iz  ],
      cd.geY[ix][iy][iz+1], cd.geXY[ix][iy][iz+1], cd.geX  [ix][iy+1][iz+1]);
    addTetraFaces( //tetra 0732 > edges 02 03 07 23 27 37
      cd.geY[ix][iy  ][iz], cd.geYZ[ix][iy  ][iz], cd.geXYZ[ix][iy  ][iz  ],
      cd.geZ[ix][iy+1][iz], cd.geXZ[ix][iy+1][iz], cd.geX  [ix][iy+1][iz+1]);
  }}}
  
  //get rid of mesh vertices stored in the grid edges
  cd.clearEdges();
  
  //create applyColorFunction method
  mesh.applyColorFunction = function(uvw){
    
    for(var i=0, il=this.vertices.length; i<il; i++){
      var p = this.vertices[i];
      p.uvw = uvw(p.fposition[0], p.fposition[1], p.fposition[2]);
    }
    
    for(var i=0, il=this.faces.length; i<il; i++){
      var f = this.faces[i];
    
      for(var j=0, jl=f.vertices.length; j<jl; j++){
        var fv = f.vertices[j];
        fv.uvw = fv.p.uvw;
      }
    }
  };
  
  //finish face properties
  for(var i=0, il=mesh.faces.length; i<il; i++){
    mesh.faces[i].texture = tex;
    mesh.faces[i].twosided = twosided;
  }
  
  //initialize and return
  mesh.createEdges();
  mesh.createCrossReferences();
  mesh.createFaceNormals();
  //mesh.createVertexNormals();
  mesh.initializeTransform();
  return mesh;
};


//========================================================
// Contour slices (3D level set)
//========================================================

Mesh.contourSlices = function(contourData, c){
  
  var cd = contourData;
  var mesh = new Mesh();
  
  //create edges from level set vertices
  //-----------------------------
  //triangle is made up of grid vertices 0,1,2; argument eij is edge object from grid vertex i to j
  var addTriEdges = function(e01, e12, e02){
    //tri vertices
    var tv = [ e01.gv0, e01.gv1, e02.gv1 ];
    
    //connect level set vertices sitting on edges incident to tv[i]
    var levelEdge = function(i){
    
      //get level set vertices along edges incident to tv[i]
      var vp, vq;
      switch(i){
      case 0: vp = e01.getp(c, mesh); vq = e02.getp(c, mesh); break;
      case 1: vp = e01.getp(c, mesh); vq = e12.getp(c, mesh); break;
      case 2: vp = e02.getp(c, mesh); vq = e12.getp(c, mesh); break;
      }
      
      //create the mesh edge
      mesh.edges.push({p:vp, q:vq}); 
    };
    
    //may or may not have an edge, depending on number of negative vertices
    var n = 0, ipos = 0, ineg = 0;
    for(var i=0; i<3; i++){
      if(tv[i].fvalue < c){ n++; ineg=i; }
      else{ ipos=i; }
    }
    
    switch(n){
    case 0: return;
    case 3: return;
    case 1: levelEdge(ineg); break;
    case 2: levelEdge(ipos); break;
    }
  };
  
  //create YZ-aligned contour edges
  for(var ix=0; ix<cd.nx  ; ix++){ 
  for(var iy=0; iy<cd.ny-1; iy++){ 
  for(var iz=0; iz<cd.nz-1; iz++){ 
    addTriEdges(cd.geY[ix][iy][iz], cd.geZ[ix][iy+1][iz], cd.geYZ[ix][iy][iz]);
    addTriEdges(cd.geZ[ix][iy][iz], cd.geY[ix][iy][iz+1], cd.geYZ[ix][iy][iz]);
  }}}
  
  //create XZ-aligned contour edges
  for(var ix=0; ix<cd.nx-1; ix++){
  for(var iy=0; iy<cd.ny  ; iy++){ 
  for(var iz=0; iz<cd.nz-1; iz++){ 
    addTriEdges(cd.geX[ix][iy][iz], cd.geZ[ix+1][iy][iz], cd.geXZ[ix][iy][iz]);
    addTriEdges(cd.geZ[ix][iy][iz], cd.geX[ix][iy][iz+1], cd.geXZ[ix][iy][iz]);
  }}}
  
  //create XY-aligned contour edges
  for(var ix=0; ix<cd.nx-1; ix++){
  for(var iy=0; iy<cd.ny-1; iy++){ 
  for(var iz=0; iz<cd.nz  ; iz++){ 
    addTriEdges(cd.geX[ix][iy][iz], cd.geY[ix+1][iy][iz], cd.geXY[ix][iy][iz]);
    addTriEdges(cd.geY[ix][iy][iz], cd.geX[ix][iy+1][iz], cd.geXY[ix][iy][iz]);
  }}}
  
  //get rid of mesh vertices stored in the grid edges
  cd.clearEdges();
  
  mesh.initializeTransform();
  return mesh;
};

