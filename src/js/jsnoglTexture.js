
//========================================================
// TextureFromSRC
//--------------------------------------------------------
// In general a Texture is any object with a setAsColor(target, u, v) property
// This creates a texture from URL, with optional bilinear filtering
// calls textureChanged when image finished loading or fails loading (i.e. to update render)
//========================================================

function TextureFromSRC(src, bilinear, textureChanged){
  var texture = this;
  
  this.width = 1;
  this.height = 1;
  
  this.alphaOverride = false;
  this.alpha = 255;
  
  //default color sampling function
  this.setAsColor = function(target, u, v, w){
    target[0] = v*255 % 255;
    target[1] = u*255 % 255;
    target[2] = 128;
    target[3] = this.alphaOverride ? this.alpha : 255;
  };
  
  //load texture  
  var image = new Image();
  
  image.onerror = function(){
    //update color sampling function to indicate error
    texture.setAsColor = function(target, u, v, w){
      target[0] = 255;
      target[1] = u*255 % 255;
      target[2] = v*255 % 255;
      target[3] = this.alphaOverride ? this.alpha : 255;
    };
    
    textureChanged();
  };
  
  image.onload = function(){
    //draw the image to an off-screen canvas
    var texCanvas = document.createElement('canvas');
    texCanvas.width = this.width;
    texCanvas.height = this.height;
    
    var texContext = texCanvas.getContext('2d');
    texContext.drawImage(this, 0, 0);
    
    //store image size
    texture.width = this.width;
    texture.height = this.height;
    
    //update color sampling function
    var texBuffer = texContext.getImageData(0, 0, this.width, this.height).data;
    
    var cblerp = function(target, ut, vt, c00, c10, c11, c01){
      var w00 = (1-ut)*(1-vt), w10 = ut*(1-vt), w11 = ut*vt, w01 = (1-ut)*vt;
      
      target[0] = c00[0]*w00 + c10[0]*w10 + c11[0]*w11 + c01[0]*w01;
      target[1] = c00[1]*w00 + c10[1]*w10 + c11[1]*w11 + c01[1]*w01;
      target[2] = c00[2]*w00 + c10[2]*w10 + c11[2]*w11 + c01[2]*w01;
      target[3] = c00[3]*w00 + c10[3]*w10 + c11[3]*w11 + c01[3]*w01;
    }
    
    var setAsColorBilinear = function(target, u, v, w){
      var up = u * this.width,  upi = Math.floor(up);
      var vp = v * this.height, vpi = Math.floor(vp);
      
      var i00 = 4*((vpi+0)*this.width + (upi+0)),
          i10 = 4*((vpi+0)*this.width + (upi+1)),  
          i11 = 4*((vpi+1)*this.width + (upi+1)),
          i01 = 4*((vpi+1)*this.width + (upi+0));
      
      cblerp(target, up - upi, vp - vpi,
        [ texBuffer[i00+0], texBuffer[i00+1], texBuffer[i00+2], this.alphaOverride ? this.alpha : texBuffer[i00+3] ], 
        [ texBuffer[i10+0], texBuffer[i10+1], texBuffer[i10+2], this.alphaOverride ? this.alpha : texBuffer[i10+3] ], 
        [ texBuffer[i11+0], texBuffer[i11+1], texBuffer[i11+2], this.alphaOverride ? this.alpha : texBuffer[i11+3] ], 
        [ texBuffer[i01+0], texBuffer[i01+1], texBuffer[i01+2], this.alphaOverride ? this.alpha : texBuffer[i01+3] ]);
    };
    
    var setAsColorNoBilinear = function(target, u, v, w){
      //clamp and round
      if(u < 0){ u = 0; }else if(u > 1){ u = 1; }
      if(v < 0){ v = 0; }else if(v > 1){ v = 1; }
      u = (u * this.width) | 0; //floor
      v = (v * this.height) | 0; //floor
      //read color
      var i = 4*(v*this.width + u);
      target[0] = texBuffer[i+0];
      target[1] = texBuffer[i+1];
      target[2] = texBuffer[i+2];
      target[3] = this.alphaOverride ? this.alpha : texBuffer[i+3];
    };
    
    texture.setAsColor = bilinear ? setAsColorBilinear : setAsColorNoBilinear;
    
    textureChanged();
  };
  
  image.src = src;
}; 


//========================================================
// TextureSkybox
//--------------------------------------------------------
// for use with Rasterizer.fillSkybox
//========================================================

var TextureSkybox = function(texSkyboxUP, texSkyboxDN, texSkyboxFT, texSkyboxBK, texSkyboxLF, texSkyboxRT){

  return function(pColor, wx, wy, wz){
    var abswx = (wx > 0) ? wx : -wx;
    var abswy = (wy > 0) ? wy : -wy;
    var abswz = (wz > 0) ? wz : -wz;
    var u,v;
    
    //x+ or x- face
    if((abswx > abswy) && (abswx > abswz)){ 
        u = 0.5*(1+wz/wx); v = 0.5*(1-wy/abswx); 
        if(wx > 0){ texSkyboxRT.setAsColor(pColor, u, v, 0); }
        else{ texSkyboxLF.setAsColor(pColor, u, v, 0); }
    }
    //z+ or z- face
    else if(abswz > abswy){ 
        u = 0.5*(1-wx/wz); v = 0.5*(1-wy/abswz);
        if(wz > 0){ texSkyboxBK.setAsColor(pColor, u, v, 0); }
        else{ texSkyboxFT.setAsColor(pColor, u, v, 0); }
    }
    //y+ or y- face
    else{ 
        u = 0.5*(1+wz/wy); v = 0.5*(1+wx/wy);
        if(wy > 0){ texSkyboxUP.setAsColor(pColor, u, v, 0); }
        else{ texSkyboxDN.setAsColor(pColor, u, v, 0); }
    }
  };
};

