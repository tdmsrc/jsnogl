
//=======================================================
// OBJECTS AND OPTIONS
//=======================================================

//canvas, buffer, and rasterizer 
//-----------------------------------------------
//main canvas
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var image = context.getImageData(0, 0, canvas.width, canvas.height);
var rast = new Rasterizer(image.data, canvas.width, canvas.height);

//mini canvas (target for raytracer)
var canvasMini = document.getElementById("canvasMini");
var contextMini = canvasMini.getContext("2d");
var imageMini = contextMini.getImageData(0, 0, canvasMini.width, canvasMini.height);
var rastMini = new Rasterizer(imageMini.data, canvasMini.width, canvasMini.height);

//some draw options
//-----------------------------------------------
var RenderObject = Object.freeze({
  NONE: "none",
  MESH: "mesh",
  DENSITY: "density"
});
var renderObject = RenderObject.NONE;

var triggerLQDraw = false;
var triggerHQDraw = true;

var renderOpts = {
  showSlices: true,
  sliceAlpha: 32,
  showOutline: true,
  showSkybox: false,
  lightModel: LightModel.PHONG,
  modeGrayscale: false,
  modeAnaglyph: false,
  flipAnaglyph: false,
  showAxes: true,
  showLabels: true
};

var ANAGLYPH_OFFSET = 0.15;
var RAY_SAMPLES_HQ = 100;
var RAY_SAMPLES_LQ = 30;
var raytraceAlphaModel = AlphaModel.BANDS;

context.font = "12px Arial";
context.textAlign = "center";
context.textBaseline = "middle";
context.fillStyle = "rgb(0,0,0)"; //"rgb(255,255,255)";

var fillColor = [255,255,255,255];//[64,64,64,64];
var coordsColor = [0,0,0,128];
var outlineColor = [0,0,0,255];

//camera and light
//-----------------------------------------------
var camera = new Camera();
var light = { ambcoeff: 0.1, diffcoeff: 1.2, speccoeff: 0.2, specexp: 8.0, pos: [0,0,0], dir: [0,0,-1] };

camera.fovy = 60;
camera.zNear = 0.1; camera.zFar = 100;
var camTheta = 1, camPhi = 0.3, camDist = 6;

var updateCamera = function(){
  //update camera based on angles and distance
  camera.position = [Math.cos(camTheta)*Math.cos(camPhi), Math.sin(camPhi), -Math.sin(camTheta)*Math.cos(camPhi)];
  Vector3d.scale(camera.position, camDist);
  Vector3d.setEqualTo(camera.forward, camera.position);
  Vector3d.scale(camera.forward, -1);
  Vector3d.normalize(camera.forward);
  camera.updateRotation();
  
  //update light based on camera orientation
  Vector3d.setEqualTo(light.pos, camera.position);
  Vector3d.setAsLinearCombination(light.pos, light.pos, 1, camera.side, 1.2);
  Vector3d.setAsLinearCombination(light.pos, light.pos, 1, camera.up, 1.2);
  
  Vector3d.setEqualTo(light.dir, light.pos);
  Vector3d.scale(light.dir, -1); 
  Vector3d.normalize(light.dir); 
};
updateCamera();

//textures and skybox
//-----------------------------------------------
var defaultTex3d = { setAsColor: function(target, u, v, w){
  target[0] = 96 + u*128 % 128;
  target[1] = 96 + v*128 % 128;
  target[2] = 96 + w*128 % 128;
  target[3] = this.alphaOverride ? this.alpha : 255;
}};

var onTexLoad = function(){ triggerHQDraw = true; }

var texTile = new TextureFromSRC("image/tile.png", true, onTexLoad);
var texWood = new TextureFromSRC("image/wood.png", true, onTexLoad);

var texSkyboxUP = new TextureFromSRC("image/miramar_up512.jpg", false, onTexLoad);
var texSkyboxDN = new TextureFromSRC("image/miramar_dn512.jpg", false, onTexLoad);
var texSkyboxFT = new TextureFromSRC("image/miramar_bk512.jpg", false, onTexLoad); //bk and ft seem switched in originals
var texSkyboxBK = new TextureFromSRC("image/miramar_ft512.jpg", false, onTexLoad);
var texSkyboxLF = new TextureFromSRC("image/miramar_lf512.jpg", false, onTexLoad);
var texSkyboxRT = new TextureFromSRC("image/miramar_rt512.jpg", false, onTexLoad);

var skyboxTex = TextureSkybox(texSkyboxUP, texSkyboxDN, texSkyboxFT, texSkyboxBK, texSkyboxLF, texSkyboxRT);


//=======================================================
// PRE-RENDERING/MESH CREATION
//=======================================================

//initial (pre-"prepare" method) coordinate box
var coordbox = new CoordinateBox({
  outx: [-2,2], rangex: [-2,2], labelx: "x", 
  outy: [-2,2], rangey: [-2,2], labely: "y",
  outz: [-2,2], rangez: [-2,2], labelz: "z"
});

var raytraceOpts; //stores some pre-computed info for DensityRaytracer
var meshLQ = new Mesh(); //mesh rendered during mousedrag
var meshHQ = new Mesh(); //mesh rendered before/after mousedrag
var meshdecor = new Mesh(); //slices etc, rendered with meshHQ


//graph
//-----------------------------------------------
var prepareGraph = function(fexpr){
  
  eval("var f = function(x,y){ return " + fexpr + "; };");
  var graphOpts = { 
    outx: [-2,2], outz: [-2,2], 
    outy: $("#graphflatten").is(':checked') ? [-0.01, 0.01] : [-1,1],
    inx: [parseFloat($("#graphxmin").val()), parseFloat($("#graphxmax").val())], 
    iny: [parseFloat($("#graphymin").val()), parseFloat($("#graphymax").val())], 
    nx: 50, ny: 50, f: f 
  };
  
  var mesh = Mesh.graph(graphOpts, defaultTex3d);
  meshLQ = mesh;
  meshHQ = mesh;
  meshdecor = Mesh.graphSlices(mesh, 15, 15, 15);
  
  coordbox = new CoordinateBox(mesh.coordBoxOpts);
};

//contours
//-----------------------------------------------
var prepareContour3d = function(fexpr){

  eval("var f = function(x,y,z){ return " + fexpr + "; };");
  var contourOpts = {
    outx: [-2, 2], outy: [-2, 2], outz: [-2, 2],
    inx: [parseFloat($("#contourxmin").val()), parseFloat($("#contourxmax").val())], 
    iny: [parseFloat($("#contourymin").val()), parseFloat($("#contourymax").val())], 
    inz: [parseFloat($("#contourzmin").val()), parseFloat($("#contourzmax").val())],
    nx: 24, ny: 24, nz: 24, f: f
  };
  var cd = Mesh.contourData(contourOpts);
  
  var meshcontour = Mesh.contour(cd, 0, defaultTex3d, true);
  meshLQ = meshcontour;
  meshHQ = meshcontour;
  meshdecor = Mesh.contourSlices(cd, 0);
  
  coordbox = new CoordinateBox(cd.coordBoxOpts);
};

//density raytrace
//-----------------------------------------------
var prepareDensity = function(fexpr){
  
  eval("var f = function(x,y,z){ return " + fexpr + "; };");
  raytraceOpts = { 
    outx: [-2, 2], outy: [-2, 2], outz: [-2, 2],
    inx: [parseFloat($("#densityxmin").val()), parseFloat($("#densityxmax").val())], 
    iny: [parseFloat($("#densityymin").val()), parseFloat($("#densityymax").val())], 
    inz: [parseFloat($("#densityzmin").val()), parseFloat($("#densityzmax").val())],
    f: f
  };
  DensityRaytracer.prepare(raytraceOpts);
  
  coordbox = new CoordinateBox({
    outx: raytraceOpts.outx, rangex: raytraceOpts.inx, labelx: "x", 
    outy: raytraceOpts.outy, rangey: raytraceOpts.iny, labely: "y",
    outz: raytraceOpts.outz, rangez: raytraceOpts.inz, labelz: "z"
  });
};

//obj mesh
//-----------------------------------------------
var prepareObj = function(objdata){

  var meshobj = Mesh.obj(
    { outx: [-2.5, 2.5], outy: [-2.5, 2.5], outz: [-2.5, 2.5] },
    objdata, texWood, true);
  meshLQ = meshobj;
  meshHQ = meshobj;
  
  meshdecor = new Mesh();
  
  coordbox = new CoordinateBox(meshobj.coordBoxOpts);
};


//=======================================================
// RENDERING
//=======================================================

//draw mesh and coordbox in low-quality style
//-------------------------------------------------------
var drawLQ = function(rast, context, image){
  
  rast.setBlendMode(true);
  meshLQ.edgeStyle = Mesh.EDGESTYLE.OUTLINE;
  meshLQ.edgeColor = outlineColor;
  meshLQ.renderEdges(rast, camera);
  
  rast.setBlendMode(true);
  coordbox.mesh.edgeColor = coordsColor;
  if(renderOpts.showAxes){ coordbox.renderBack(rast, camera); }
};

//draw mesh and coordbox in high-quality style
//-------------------------------------------------------
var drawHQ = function(rast, context, image, light){
  
  rast.setBlendMode(false);
  meshHQ.faceFillStyle = renderOpts.lightModel;
  meshHQ.render(rast, camera, light);
  
  if(renderOpts.showOutline){ rast.outline([0,0,0], 0.25); }
  
  rast.setBlendMode(true);
  meshdecor.edgeStyle = Mesh.EDGESTYLE.PLAIN;
  meshdecor.edgeColor = [0,0,0,renderOpts.sliceAlpha];
  if(renderOpts.showSlices){ meshdecor.renderEdges(rast, camera); }
  
  rast.setBlendMode(true);
  coordbox.mesh.edgeColor = coordsColor;
  if(renderOpts.showAxes){ coordbox.renderBack(rast, camera); }
};

//draw coordbox only
//-------------------------------------------------------
var drawNothing = function(rast, context, image){
  var timeStart = Date.now();
  rast.prepare(camera);  
  
  //draw background (skybox or fill)
  //-------------------------------------
  if(renderOpts.modeGrayscale){ rast.enableLuminanceMode(7); }
  else{ rast.disableLuminanceMode(); }
  
  if(renderOpts.showSkybox){ rast.fillSkybox(skyboxTex); }
  else{ rast.fill(fillColor); }
  rast.clearZBuffer();
  
  //draw coordbox
  //-------------------------------------
  rast.setBlendMode(true);
  coordbox.mesh.edgeColor = coordsColor;
  if(renderOpts.showAxes){ coordbox.renderBack(rast, camera); }

  context.putImageData(image, 0, 0);
  
  //finish
  //-------------------------------------
  if(renderOpts.showLabels){ coordbox.renderLabels(rast, camera, context); }
  
  var dt = Date.now() - timeStart;
  if(triggerHQDraw){ $("#output").html("HQ render: " + dt + "ms"); }
  
  triggerHQDraw = false;
  triggerLQDraw = false;
};

//draw (mesh, non anaglyph)
//-------------------------------------------------------
var draw = function(rast, context, image, light){
  var timeStart = Date.now();
  rast.prepare(camera);  
  
  //draw background (skybox or fill)
  //-------------------------------------
  if(renderOpts.modeGrayscale){ rast.enableLuminanceMode(7); }
  else{ rast.disableLuminanceMode(); }
  
  if(renderOpts.showSkybox){ rast.fillSkybox(skyboxTex); }
  else{ rast.fill(fillColor); }
  rast.clearZBuffer();
  
  //draw mesh+coordbox
  //-------------------------------------
  if(triggerHQDraw){ drawHQ(rast, context, image, light); }
  else if(triggerLQDraw){ drawLQ(rast, context, image); }

  context.putImageData(image, 0, 0);
  
  //finish
  //-------------------------------------
  if(renderOpts.showLabels){ coordbox.renderLabels(rast, camera, context); }
  
  var dt = Date.now() - timeStart;
  if(triggerHQDraw){ $("#output").html("HQ render: " + dt + "ms"); }
  
  triggerHQDraw = false;
  triggerLQDraw = false;
};

//draw (mesh, anaglyph)
//-------------------------------------------------------
var drawAnaglyph = function(rast, context, image, light){
  var timeStart = Date.now();  
  rast.prepare(camera);  
  
  //draw background (skybox or fill)
  //-------------------------------------
  rast.enableLuminanceMode(7);
  if(renderOpts.showSkybox){ rast.fillSkybox(skyboxTex); }
  else{ rast.fill(fillColor); }
  
  //render mesh+coordbox, overlaying left and right perspectives
  //-------------------------------------
  //save camera position
  var camPos = [0,0,0]; Vector3d.setEqualTo(camPos, camera.position);
  
  //draw L
  rast.enableLuminanceMode(renderOpts.flipAnaglyph ? 6 : 1); //1 = 001_2 = R
  rast.clearZBuffer();
  Vector3d.setAsLinearCombination(camera.position, camPos, 1, camera.side, -ANAGLYPH_OFFSET);
  if(triggerHQDraw){ drawHQ(rast, context, image, light); }
  else if(triggerLQDraw){ drawLQ(rast, context, image); }
  
  //draw R  
  rast.enableLuminanceMode(renderOpts.flipAnaglyph ? 1 : 6); //6 = 110_2 = BG
  rast.clearZBuffer();
  Vector3d.setAsLinearCombination(camera.position, camPos, 1, camera.side, ANAGLYPH_OFFSET);
  if(triggerHQDraw){ drawHQ(rast, context, image, light); }
  else if(triggerLQDraw){ drawLQ(rast, context, image); }
  
  //restore camera position
  camera.position = camPos;

  context.putImageData(image, 0, 0);
  
  //finish
  //-------------------------------------
  if(renderOpts.showLabels){ coordbox.renderLabels(rast, camera, context); }
  
  var dt = Date.now() - timeStart;
  if(triggerHQDraw){ $("#output").html("HQ render: " + dt + "ms"); }
  
  triggerHQDraw = false;
  triggerLQDraw = false;
};

//draw (raytracer--ignores mesh)
//-------------------------------------------------------
var drawRaytrace = function(rast, context, image){
  var timeStart = Date.now();
  rast.prepare(camera);
  
  //draw background (skybox or fill) and coordbox
  //-------------------------------------  
  if(renderOpts.modeGrayscale){ rast.enableLuminanceMode(7); }
  else{ rast.disableLuminanceMode(); }
  
  if(renderOpts.showSkybox){ rast.fillSkybox(skyboxTex); }
  else{ rast.fill(fillColor); }
  rast.clearZBuffer();
  
  rast.setBlendMode(true);
  coordbox.mesh.edgeColor = coordsColor;
  if(renderOpts.showAxes){ coordbox.renderBack(rast, camera); }
  
  context.putImageData(image, 0, 0);
  
  //raytrace density onto mini canvas
  //-------------------------------------
  rastMini.prepare(camera);
  
  if(renderOpts.modeGrayscale){ rastMini.enableLuminanceMode(7); }
  else{ rastMini.disableLuminanceMode(); }
  
  raytraceOpts.alphaModel = raytraceAlphaModel;
  raytraceOpts.nr = triggerHQDraw ? RAY_SAMPLES_HQ : RAY_SAMPLES_LQ;
  DensityRaytracer.raytrace(rastMini, camera, raytraceOpts);
  
  contextMini.putImageData(imageMini, 0, 0);
  
  //overlay density onto main canvas
  //-------------------------------------
  context.save();
  context.scale(canvas.width/canvasMini.width, canvas.height/canvasMini.height);
  context.drawImage(canvasMini, 0, 0);
  context.restore();
  
  //finish  
  //-------------------------------------
  if(renderOpts.showLabels){ coordbox.renderLabels(rast, camera, context); }

  var dt = Date.now() - timeStart;
  if(triggerHQDraw){ $("#output").html("HQ render: " + dt + "ms"); }
  
  triggerHQDraw = false;
  triggerLQDraw = false;
};


//=======================================================
// JQUERY DOCUMENT READY
//=======================================================

$(document).ready(function(){

  //insert example OBJ file
  //-------------------------------------
  $("#dataObj").val(StaticTextRC["twisted_pillar.obj"]);
  
  //canvas mouse handler
  //-------------------------------------
  $("#canvas").bind("contextmenu", function(event){ event.preventDefault(); });
  
  $("#canvas").addMouseDragHandler({
    //onDragStart:  function(p, b){ triggerLQDraw = true; },
    onDragEnd:    function(p, b){ triggerHQDraw = true; },
    
    onDrag: function(p, dp, b){
      camTheta -= 0.01*dp[0];
      camPhi += 0.01*dp[1];
      if(camPhi > 1.57){ camPhi = 1.57; }else if(camPhi < -1.57){ camPhi = -1.57; }
      
      updateCamera();  
      triggerLQDraw = true;
    }
  }, 4); //mouse drag tolerance
  
  //"create" button handlers
  //-------------------------------------
  //create graph
  $("#createGraph").click(function(){
    prepareGraph( $("#fexprGraph").val() );
    renderObject = RenderObject.MESH;
    triggerHQDraw = true;
  });
  
  //create contour3d
  $("#createContour3d").click(function(){
    prepareContour3d( $("#fexprContour3d").val() );
    renderObject = RenderObject.MESH;
    triggerHQDraw = true;
  });
  
  //prepare density
  $("#createDensity").click(function(){
    prepareDensity( $("#fexprDensity").val() );
    renderObject = RenderObject.DENSITY;
    triggerHQDraw = true;
  });

  //create obj
  $("#createObj").click(function(){
    var objdata = $("#dataObj").val().split('\n');
    prepareObj(objdata);
    renderObject = RenderObject.MESH;
    triggerHQDraw = true;
  });
  
  //render options: defaults
  //-------------------------------------
  var resetRenderOptionsUI = function(){
    //TODO
    //update UI components from renderOpts values
  };
  resetRenderOptionsUI();
  
  //render options: handlers
  //-------------------------------------
  $('#optSlices').change(function(){
    renderOpts.showSlices = $(this).is(':checked');
    triggerHQDraw = true;
  });
  
  $('#optSliceAlpha').change(function(){
    var a = parseFloat($(this).val());
    if(a < 0){ a = 0; }else if(a > 1){ a = 1; }
    $(this).val(a.toString());
    
    renderOpts.sliceAlpha = Math.round(255*a);
    triggerHQDraw = true;
  });
  
  $('#optOutline').change(function(){
    renderOpts.showOutline = $(this).is(':checked');
    triggerHQDraw = true;
  });
  
  $('#optSkybox').change(function(){
    renderOpts.showSkybox = $(this).is(':checked');
    triggerHQDraw = true;
  });
  
  $('#optFlipAnaglyph').change(function(){
    renderOpts.flipAnaglyph = $(this).is(':checked');
    if(renderOpts.modeAnaglyph){ triggerHQDraw = true; }
  });
  
  $('#optAxes').change(function(){
    renderOpts.showAxes = $(this).is(':checked');
    triggerHQDraw = true;
  });
  
  $('#optLabels').change(function(){
    renderOpts.showLabels = $(this).is(':checked');
    triggerHQDraw = true;
  });
  
  $('#optLightModel').change(function(){
    switch($(this).val()){
    case "NODRAW":  renderOpts.lightModel = LightModel.NODRAW;  break;
    case "NOLIGHT": renderOpts.lightModel = LightModel.NOLIGHT; break;
    case "FLAT":    renderOpts.lightModel = LightModel.FLAT;    break;
    case "GOURAUD": renderOpts.lightModel = LightModel.GOURAUD; break;
    case "CEL":     renderOpts.lightModel = LightModel.CEL;     break;
    case "PHONG":   renderOpts.lightModel = LightModel.PHONG;   break;
    }
    triggerHQDraw = true;
  });
  
  $('#optRenderMode').change(function(){
    switch($(this).val()){
    case "NORMAL":    renderOpts.modeGrayscale = false; renderOpts.modeAnaglyph = false;  break;
    case "GRAYSCALE": renderOpts.modeGrayscale = true;  renderOpts.modeAnaglyph = false;  break;
    case "ANAGLYPH":  renderOpts.modeGrayscale = false; renderOpts.modeAnaglyph = true;   break;
    }
    triggerHQDraw = true;
  });
  
  $('#optDensityAlphaMode').change(function(){
    switch($(this).val()){
    case "SMOOTH": raytraceAlphaModel = AlphaModel.SMOOTH;  break;
    case "BANDS":  raytraceAlphaModel = AlphaModel.BANDS;   break;
    }
    triggerHQDraw = true;
  });
  
  //start main loop
  //-------------------------------------
  window.setInterval(function(){
    if(!triggerHQDraw && !triggerLQDraw){ return; }
    
    //render
    switch(renderObject){
    case RenderObject.MESH:
      if(renderOpts.modeAnaglyph){ drawAnaglyph(rast, context, image, light); }
      else{ draw(rast, context, image, light); }
      break;
    case RenderObject.DENSITY: drawRaytrace(rast, context, image); break;
    default: drawNothing(rast, context, image);
    }
    
  }, 10);
});

