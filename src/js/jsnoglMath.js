
//========================================================
// Vectors and Matrices
//========================================================
//temp values
var tempx=0, tempy=0, tempz=0, tempw=0;

//specify a Vector3d as [x,y,z]
function Vector3d(){ };

Vector3d.copy = function(u){ return [u[0], u[1], u[2]]; }
Vector3d.setEqualTo = function(u, v){ u[0]=v[0]; u[1]=v[1]; u[2]=v[2]; }

Vector3d.add = function(u, v){ u[0]+=v[0]; u[1]+=v[1]; u[2]+=v[2]; };
Vector3d.subtract = function(u, v){ u[0]-=v[0]; u[1]-=v[1]; u[2]-=v[2]; };
Vector3d.scale = function(u, t){ u[0]*=t; u[1]*=t; u[2]*=t; };

Vector3d.dot = function(u, v){ return u[0]*v[0] + u[1]*v[1] + u[2]*v[2]; };
Vector3d.invlen = function(u){ return 1.0 / Math.sqrt(Vector3d.dot(u, u)); };
Vector3d.normalize = function(u){ Vector3d.scale(u, Vector3d.invlen(u)); };

Vector3d.setAsLinearCombination = function(target, u, s, v, t){ 
  target[0] = u[0]*s+v[0]*t;
  target[1] = u[1]*s+v[1]*t;
  target[2] = u[2]*s+v[2]*t;
}
Vector3d.setAsCross = function(target, u, v){ 
  tempx = u[1]*v[2]-u[2]*v[1];
  tempy = u[2]*v[0]-u[0]*v[2];
  tempz = u[0]*v[1]-u[1]*v[0];
  
  target[0]=tempx; target[1]=tempy; target[2]=tempz;
};

//--------------------------------------------------------

//specify a Matrix as [r0,...,rn] where ri are row vectors (so m[i][j] = row i, col j)
function Matrix3d(){ };

//matrix3d times vector3d
Matrix3d.setAsTransform = function(target, m, v){
  tempx = m[0][0]*v[0] + m[0][1]*v[1] + m[0][2]*v[2];
  tempy = m[1][0]*v[0] + m[1][1]*v[1] + m[1][2]*v[2];
  tempz = m[2][0]*v[0] + m[2][1]*v[1] + m[2][2]*v[2];
    
  target[0]=tempx; target[1]=tempy; target[2]=tempz;
};

//imitate glRotate matrix
Matrix3d.glRotate = function(axis, angle){

  var c = Math.cos(angle), s = Math.sin(angle);
  var n = Vector3d.copy(axis); Vector3d.normalize(n);

  return [
    [ (1-c)*n[0]*n[0] + c,      (1-c)*n[0]*n[1] - s*n[2], (1-c)*n[0]*n[2] + s*n[1] ],
    [ (1-c)*n[1]*n[0] + s*n[2], (1-c)*n[1]*n[1] + c,      (1-c)*n[1]*n[2] - s*n[0] ],
    [ (1-c)*n[2]*n[0] - s*n[1], (1-c)*n[2]*n[1] + s*n[0], (1-c)*n[2]*n[2] + c      ]
  ];
};

//--------------------------------------------------------

//specify a Matrix as [r0,...,rn] where ri are row vectors (so m[i][j] = row i, col j)
function Matrix4d(){ };

//matrix4d times [vx, vy, vz, 1], then divides by output w value
Matrix4d.setAsTransformProjective = function(target, m, v){
  tempx = m[0][0]*v[0] + m[0][1]*v[1] + m[0][2]*v[2] + m[0][3];
  tempy = m[1][0]*v[0] + m[1][1]*v[1] + m[1][2]*v[2] + m[1][3];
  tempz = m[2][0]*v[0] + m[2][1]*v[1] + m[2][2]*v[2] + m[2][3];
  tempw = 1.0 / (m[3][0]*v[0] + m[3][1]*v[1] + m[3][2]*v[2] + m[3][3]);
  
  target[0]=tempx*tempw; target[1]=tempy*tempw; target[2]=tempz*tempw;
};

//imitate gluPerspective matrix (note: fovy is in degrees, aspect = width/height)
Matrix4d.gluPerspective = function(fovy, aspect, zNear, zFar){
  
  var h = zNear * Math.tan(Math.PI * fovy / 360.0),
      w = h * aspect,
      invdz = 1.0 / (zNear - zFar);
  
  return [[zNear/w,0,0,0], [0,zNear/h,0,0], [0,0,(zNear+zFar)*invdz,2*zNear*zFar*invdz], [0,0,-1,0]];
};


//========================================================
// Camera
//--------------------------------------------------------
// Instructions:
// - Any time position or forward is changed, call updateRotation()
// - Any time aspect, fovy, zNear, or zFar is changed, call updateProjection()
// - Note: rasterizer.prepare() automatically sets aspect and invokes updateProjection()
// - Transform points using worldToEye() and eyeToClip()
//========================================================

function Camera(){
  
  //camera position and orientation
  this.position = [0,0,0];
  this.forward = [0,0,-1]; //roll = rotation around this vector
  this.fixedUp = [0,1,0]; //yaw = rotation around this vector
  
  this.side = [1,0,0];
  this.up = [0,1,0];
  
  //perspective options
  this.aspect = 1.0; // = width/height
  this.fovy = 60; //in degrees
  this.zNear = 0.1;
  this.zFar = 100.0;
  
  //initialize computed values
  this.updateRotation();
  this.updateProjection();
};

//recompute side, up, and matrixRotation
Camera.prototype.updateRotation = function(){

  Vector3d.setAsCross(this.side, this.forward, this.fixedUp); Vector3d.normalize(this.side);
  Vector3d.setAsCross(this.up, this.side, this.forward);
  
  this.matrixRotation = [
    [ this.side[0], this.side[1], this.side[2] ],
    [ this.up[0], this.up[1], this.up[2] ],
    [ -this.forward[0], -this.forward[1], -this.forward[2] ]
  ];
};

//recompute matrixProjection
Camera.prototype.updateProjection = function(){
  this.matrixProjection = Matrix4d.gluPerspective(this.fovy, this.aspect, this.zNear, this.zFar);
};

//rotate "yaw" radians about fixedUp and "pitch" radians about "side"
Camera.prototype.rotate = function(yaw, pitch){
  
  //rotate forward vector according to pitch
  Vector3d.setAsLinearCombination(this.forward, this.forward, Math.cos(pitch), this.up, Math.sin(pitch));
  
  //then rotate the result according to yaw
  var m = Matrix3d.glRotate(this.fixedUp, yaw);
  Matrix3d.setAsTransform(this.forward, m, this.forward);
  Vector3d.normalize(this.forward);
  
  this.updateRotation();
};

//gives "eye coordinates" from "world coordinates"
Camera.prototype.setAsWorldToEye = function(target, p){

  Vector3d.setEqualTo(target, p);
  Vector3d.subtract(target, this.position);
  Matrix3d.setAsTransform(target, this.matrixRotation, target);
};

//gives "clip coordinates" from "eye coordinates"
Camera.prototype.setAsEyeToClip = function(target, p){

  Matrix4d.setAsTransformProjective(target, this.matrixProjection, p);
};

