// REQUIRES: 
// - jsnoglMath.js

//========================================================
// Rasterizer for 3D lines and triangles
// Computes perspective-correct interpolation values
//--------------------------------------------------------
// Instructions:
// Step 1) Create the Rasterizer
// Step 2) Call rasterizer.prepare(camera)
// Step 3) Use camera transformation to set "clip" and "eye" properties for each point
// Step 4) Use rasterizer.line or rasterizer.triangle to draw lines or triangles
// Can also draw directly: call "setPosition(x,y)" and then 
//========================================================
//temp value
var pColor = [0,0,0,255];

function Rasterizer(buffer, width, height){
  
  //buffer info
  this.buffer = buffer; 
  this.zbuffer = []; //stores eye coordinate z-values
  this.width = width;
  this.height = height;
  
  //setpixel mode
  this.modeBlend = false;
  this.modeLuminance = false;
  this.channelMask = 7; //1 = r, 2 = g, 4 = b
  this.updateMode();
  
  //set raster position
  this.setPosition(0,0);
  
  //default values for screen -> eye conversion
  this.prepareScrToEye(60, this.width / this.height);
  //default values for screen -> world conversion
  this.scrToWorldCoeff1 = [0,0,0];
  this.scrToWorldCoeffx = [0,0,0];
  this.scrToWorldCoeffy = [0,0,0];
  //default znear/zfar
  this.zNear = 0.1;
  this.zFar = 100;
};

Rasterizer.prototype.prepareScrToEye = function(fovy, aspect){

  var tanfovy = Math.tan(Math.PI * fovy / 360.0); 
  
  this.xScrToEye = 2*tanfovy * aspect / this.width;
  this.xScrToEye0 = -tanfovy * aspect;
  
  this.yScrToEye = -2*tanfovy / this.height;
  this.yScrToEye0 = tanfovy;
};

//set camera's aspect to match buffer, and precompute some values for screen -> eye and screen -> world
//call this, then transform points using camera, then rasterize
Rasterizer.prototype.prepare = function(camera){
  
  //set camera's aspect to match buffer's
  camera.aspect = this.width / this.height;
  camera.updateProjection();
  
  //compute values for screen -> eye conversion
  this.prepareScrToEye(camera.fovy, camera.aspect);
  
  //compute values for screen -> world conversion
  //i.e., from screen coords (x,y), want to get vector w in world coords pointing along ray
  //this is: w = cam.fwd + cam.side*(xScrToEye0 + x*xScrToEye) + cam.up*(yScrToEye0 + y*yScrToEye)
  Vector3d.setAsLinearCombination(this.scrToWorldCoeff1, camera.side, this.xScrToEye0, camera.up, this.yScrToEye0); 
  Vector3d.add(this.scrToWorldCoeff1, camera.forward);
  
  Vector3d.setEqualTo(this.scrToWorldCoeffx, camera.side); Vector3d.scale(this.scrToWorldCoeffx, this.xScrToEye);
  Vector3d.setEqualTo(this.scrToWorldCoeffy, camera.up); Vector3d.scale(this.scrToWorldCoeffy, this.yScrToEye);
  
  //store znear and zfar
  this.zNear = camera.zNear;
  this.zFar = camera.zFar;
};

//draw a single pixel at the current position and move position to next pixel
//use: setPixelFill, setPosition, setPixel, setBlendMode, enableLuminanceMode, disableLuminanceMode
//--------------------------------------------
//change modes (blend and luminance)
Rasterizer.prototype.setBlendMode = function(enabled){
  this.modeBlend = enabled;
  this.updateMode();
};

Rasterizer.prototype.enableLuminanceMode = function(channelMask){
  if(typeof(channelMask) === 'undefined'){ channelMask = 7; }

  this.channelMask = channelMask;
  this.modeLuminance = true;
  this.updateMode();
};

Rasterizer.prototype.disableLuminanceMode = function(){
  this.modeLuminance = false;
  this.updateMode();
};

Rasterizer.prototype.updateMode = function(){
  this.setPixelFill = this.modeLuminance ? this.setPixelFill_Luminance : this.setPixelFill_NoLuminance;
  this.setPixel = this.modeLuminance ? 
    (this.modeBlend ? this.setPixel_Blend_Luminance : this.setPixel_NoBlend_Luminance) : 
    (this.modeBlend ? this.setPixel_Blend_NoLuminance : this.setPixel_NoBlend_NoLuminance);
};

//used when filling (fill, skybox, post-processing)
Rasterizer.prototype.setPixelFill_NoLuminance = function(color, bi){
  this.buffer[bi  ] = color[0]; 
  this.buffer[bi+1] = color[1]; 
  this.buffer[bi+2] = color[2]; 
  this.buffer[bi+3] = 255;
};

Rasterizer.prototype.setPixelFill_Luminance = function(color, bi){
  var l = (0.299*color[0] + 0.587*color[1] + 0.114*color[2]) | 0; //floor
  
  if(this.channelMask & 1){ this.buffer[bi  ] = l; }
  if(this.channelMask & 2){ this.buffer[bi+1] = l; }
  if(this.channelMask & 4){ this.buffer[bi+2] = l; }
  this.buffer[bi+3] = 255;
};

//used when rasterizing (lines, triangles)
Rasterizer.prototype.setPosition = function(x, y){
  this.zbufferIndex = this.width*y + x;
  this.bufferIndex = 4*this.zbufferIndex;
};

Rasterizer.prototype.setPixel_NoBlend_NoLuminance = function(color, z){
  this.buffer[this.bufferIndex++] = color[0]; 
  this.buffer[this.bufferIndex++] = color[1]; 
  this.buffer[this.bufferIndex++] = color[2]; 
  this.bufferIndex++;
  
  this.zbuffer[this.zbufferIndex++] = z;
};

Rasterizer.prototype.setPixel_NoBlend_Luminance = function(color, z){
  var l = (0.299*color[0] + 0.587*color[1] + 0.114*color[2]) | 0; //floor
  
  if(this.channelMask & 1){ this.buffer[this.bufferIndex] = l; } this.bufferIndex++;
  if(this.channelMask & 2){ this.buffer[this.bufferIndex] = l; } this.bufferIndex++;
  if(this.channelMask & 4){ this.buffer[this.bufferIndex] = l; } this.bufferIndex++;
  this.bufferIndex++;
  
  this.zbuffer[this.zbufferIndex++] = z;
};

Rasterizer.prototype.setPixel_Blend_NoLuminance = function(color, z){
  var alpha = color[3] * 0.00390625; // = color[3] / 255
  var dr = alpha * (color[0] - this.buffer[this.bufferIndex+0]),
      dg = alpha * (color[1] - this.buffer[this.bufferIndex+1]),
      db = alpha * (color[2] - this.buffer[this.bufferIndex+2]);
  
  this.buffer[this.bufferIndex++] += dr; 
  this.buffer[this.bufferIndex++] += dg; 
  this.buffer[this.bufferIndex++] += db;
  this.bufferIndex++; 
  
  this.zbuffer[this.zbufferIndex++] = z;
};

Rasterizer.prototype.setPixel_Blend_Luminance = function(color, z){
  var l = (0.299*color[0] + 0.587*color[1] + 0.114*color[2]) | 0; //floor
  
  var alpha = color[3] * 0.00390625; // = color[3] / 255
  var dr = alpha * (l - this.buffer[this.bufferIndex  ]),
      dg = alpha * (l - this.buffer[this.bufferIndex+1]),
      db = alpha * (l - this.buffer[this.bufferIndex+2]);
  
  if(this.channelMask & 1){ this.buffer[this.bufferIndex] += dr; } this.bufferIndex++;
  if(this.channelMask & 2){ this.buffer[this.bufferIndex] += dg; } this.bufferIndex++;
  if(this.channelMask & 4){ this.buffer[this.bufferIndex] += db; } this.bufferIndex++;
  this.bufferIndex++;
  
  this.zbuffer[this.zbufferIndex++] = z;
};

//fill the buffer / post-processing
//--------------------------------------------
Rasterizer.prototype.clearZBuffer = function(){
  
  var i=0;
  for(var j=0, jl=this.height*this.width; j<jl; j++){
    this.zbuffer[j] = -this.zFar;
  }
};

Rasterizer.prototype.fill = function(color){
  
  var i=0;
  for(var j=0, jl=this.height*this.width; j<jl; j++){
    this.setPixelFill(color, i);
    i+=4;
  }
};

//the argument skybox is a function(pColor, wx, wy, wz)
//should set pColor to the color in the direction of the vector w
//assumes this.prepare has been called, for correct skybox positioning
Rasterizer.prototype.fillSkybox = function(skybox){
  
  var i=0;
  var wx,wy,wz;
  var color = [0,0,0,0];
  
  for(var iy=0, iyl=this.height; iy<iyl; iy++){
    wx = this.scrToWorldCoeff1[0] + iy*this.scrToWorldCoeffy[0];
    wy = this.scrToWorldCoeff1[1] + iy*this.scrToWorldCoeffy[1];
    wz = this.scrToWorldCoeff1[2] + iy*this.scrToWorldCoeffy[2];
    
    for(var ix=0, ixl=this.width; ix<ixl; ix++){
      wx += this.scrToWorldCoeffx[0];
      wy += this.scrToWorldCoeffx[1];
      wz += this.scrToWorldCoeffx[2];
      
      skybox(color, wx, wy, wz);
      this.setPixelFill(color, i);
      i+=4;
    }
  }
};

//draw color wherever there is a jump in zbuffer values of at least threshold
Rasterizer.prototype.outline = function(color, threshold){
  var rast = this;
    
  //offsets from zbuffer index at which to lookup z values
  var dzbi = [ -1, 1, this.width, -this.width ],
      zbimax = this.width*this.height; //max zbuffer index
    
  //get difference between min and max z values at position zbi and offsets  
  var dz = function(zbi){
    var z = rast.zbuffer[zbi],
        zmin = z, zmax = z;
    
    for(var i=0, il=dzbi.length; i<il; i++){
      var zbj = zbi+dzbi[i]; //offset zbuffer index
      if(zbj < 0){ continue; } if(zbj >= zbimax){ continue; }
      
      z = rast.zbuffer[zbj];
      if(z < zmin){ zmin = z; }else if(z > zmax){ zmax = z; }
    }
    
    return zmax-zmin;
  };
  
  //for each pixel, check if dz is bigger than threshold; if so, draw color
  var bi=0;
  for(var i=0, il=this.height*this.width; i<il; i++){
      if(dz(i) > threshold){ this.setPixelFill(color, bi); }
      bi+=4;
  }
};


//LINE RASTERIZER
//--------------------------------------------
//pattern is (optional) sequence of 8 bits indicating which pixels show up in line rasterization (e.g. 51=11001100 dashed)
//--------------------------------------------
//p and q are any objects with "clip" and "eye" properties
//assumes this.prepare has been called with same camera that produced clip and eye coordinates
//fragshader takes params (pColor, lp, lq); these are perspective correct barycentric coords
//--------------------------------------------
Rasterizer.LINE_Z_BIAS = 0.02; //to avoid zbuffer rejection for lines drawn on top of polys
Rasterizer.PATTERN = {
  SOLID: 255, //11111111
  DASH:   51, //00110011
  DOT:    86  //01010101
};

Rasterizer.prototype.line = function(p, q, color, pattern){ //fragShader, pattern){
  if(typeof(pattern) === 'undefined'){ pattern = Rasterizer.PATTERN.SOLID; }
  var rast = this;
  
  //get screen coordinates
  var pi = [ Math.round((1+p.clip[0])*0.5*rast.width), Math.round((1-p.clip[1])*0.5*rast.height) ],
      qi = [ Math.round((1+q.clip[0])*0.5*rast.width), Math.round((1-q.clip[1])*0.5*rast.height) ];
  
  //clip to buffer rect or return if out of rect
  //-------------------------------------------
  var dx = qi[0] - pi[0], dy = qi[1] - pi[1];
  
  //clip by rect edges x = 0 and x = rast.width-1
  var x0 = pi[0], x1 = qi[0];
  if(dx < 0){ x0 = qi[0]; x1 = pi[0]; }
  
  if(x0 < 0){
    if(x1 < 0){ return; }
    var ri = [ 0, Math.round(pi[1] + dy*(0 - pi[0])/dx) ];
    if(dx > 0){ pi = ri; }else{ qi = ri; }
  }
  
  var xmax = rast.width-1;
  if(x1 > xmax){
    if(x0 > xmax){ return; }
    var ri = [ xmax, Math.round(pi[1] + dy*(xmax - pi[0])/dx) ];
    if(dx > 0){ qi = ri; }else{ pi = ri; }
  }
  
  //clip by rect edges y = 0 and y = rast.height-1
  var y0 = pi[1], y1 = qi[1];
  if(dy < 0){ y0 = qi[1]; y1 = pi[1]; }
  
  if(y0 < 0){ 
    if(y1 < 0){ return; }
    var ri = [ Math.round(pi[0] + dx*(0 - pi[1])/dy), 0 ];
    if(dy > 0){ pi = ri; }else{ qi = ri; }
  }
  
  var ymax = rast.height-1;
  if(y1 > ymax){
    if(y0 > ymax){ return; }
    var ri = [ Math.round(pi[0] + dx*(ymax - pi[1])/dy), ymax ];
    if(dy > 0){ qi = ri; }else{ pi = ri; }
  }
  //-------------------------------------------
  
  //used to convert eye coords -> line parameter
  var pp = Vector3d.dot(p.eye, p.eye), // |p|^2
      qq = Vector3d.dot(q.eye, q.eye), // |q|^2
      pq = Vector3d.dot(p.eye, q.eye); // p.q
  
  var lpv = [0,0,0]; Vector3d.setAsLinearCombination(lpv, p.eye,qq, q.eye,-pq); //|q|^2 (proj p onto q perp)
  var lqv = [0,0,0]; Vector3d.setAsLinearCombination(lqv, q.eye,pp, p.eye,-pq); //|p|^2 (proj q onto p perp)
  //-------------------------------------------
  
  //get dx, sgn(dx), abs(dx), and similarly for dy
  dx = qi[0] - pi[0]; var sgndx = (dx > 0) ? 1 : -1, absdx = (dx > 0) ? dx : -dx;
  dy = qi[1] - pi[1]; var sgndy = (dy > 0) ? 1 : -1, absdy = (dy > 0) ? dy : -dy;
  
  //determine if line is steep (i.e. abs(slope) > 1, or vertical)
  var steep = (absdx < absdy);
  
  //set initial point for rasterizing
  var x = pi[0], y = pi[1], err = 0;
  
  var pcount = 0; //used for pattern
  var vx = x*rast.xScrToEye + rast.xScrToEye0,
      vy = y*rast.yScrToEye + rast.yScrToEye0;
  
  if(steep){
    //err = (abs value of x-offset of current pos from line - 0.5) * absdy
    err = -(absdy >> 1); 
    
    //loop over y values
    for(var i=0, il=absdy; i<=il; i++){
      rast.setPosition(x,y); 
      
      //compute line parameter 
      var lp = vx*lpv[0] + vy*lpv[1] - lpv[2], // = [vx,vy,-1].lpv
          lq = vx*lqv[0] + vy*lqv[1] - lqv[2]; // = [vx,vy,-1].lqv
      var suminv = 1.0 / (lp + lq);
      lp *= suminv; lq *= suminv;
      
      //clipping and zbuffer
      var zEye = lp*p.eye[2] + lq*q.eye[2] + Rasterizer.LINE_Z_BIAS;
      
      var skip = false;
      if((1 << (pcount++ % 8)) & ~pattern){ skip = true; }
      else if( (-zEye > rast.zFar) || (-zEye < rast.zNear) ){ skip = true; }
      else if(rast.zbuffer[rast.zbufferIndex] > zEye){ skip = true; }
      
      if(!skip){ 
        //fragShader(pColor, lp, lq); 
        //rast.setPixel(pColor, zEye);
        rast.setPixel(color, zEye);
      }
      
      //update y value and horizontal error
      y += sgndy; err += absdx;
      vy = y*rast.yScrToEye + rast.yScrToEye0; 
      //if error exceeds threshold, update x value and horizontal error
      if(err > 0){ 
        x += sgndx; err -= absdy;
        vx = x*rast.xScrToEye + rast.xScrToEye0;
      }
    }
  }
  
  else{ 
    //err = (abs value of y-offset of current pos from line - 0.5) * absdx
    err = -(absdx >> 1); 
    
    //loop over x values
    for(var i=0, il=absdx; i<=il; i++){
      rast.setPosition(x,y); 
      
      //compute line parameter
      var lp = vx*lpv[0] + vy*lpv[1] - lpv[2], // = [vx,vy,-1].lpv
          lq = vx*lqv[0] + vy*lqv[1] - lqv[2]; // = [vx,vy,-1].lqv
      var suminv = 1.0 / (lp + lq); 
      lp *= suminv; lq *= suminv;
      
      //clipping and zbuffer
      var zEye = lp*p.eye[2] + lq*q.eye[2] + Rasterizer.LINE_Z_BIAS;
      
      var skip = false;
      if((1 << (pcount++ % 8)) & ~pattern){ skip = true; }
      else if( (-zEye > rast.zFar) || (-zEye < rast.zNear) ){ skip = true; }
      else if(rast.zbuffer[rast.zbufferIndex] > zEye){ skip = true; }
      
      if(!skip){ 
        //fragShader(pColor, lp, lq); 
        //rast.setPixel(pColor, zEye);
        rast.setPixel(color, zEye);
      }
      
      //update x value and vertical error
      x += sgndx; err += absdy; 
      vx = x*rast.xScrToEye + rast.xScrToEye0;
      //if error exceeds threshold, update y value and vertical error
      if(err > 0){ 
        y += sgndy; err -= absdx; 
        vy = y*rast.yScrToEye + rast.yScrToEye0; 
      }
    }
  }
};

//TRIANGLE RASTERIZER
//--------------------------------------------
//p, q, and r are any objects with "clip" and "eye" properties
//assumes this.prepare has been called with same camera that produced clip and eye coordinates
//fragshader takes params (pColor, lp, lq, lr); these are perspective correct barycentric coords
//--------------------------------------------
//temp values
var qxr = [0,0,0], rxp = [0,0,0], pxq = [0,0,0];

Rasterizer.prototype.triangle = function(p, q, r, fragShader){
  var rast = this;
  
  //get screen coordinates
  var pi = [ Math.round((1+p.clip[0])*0.5*rast.width), Math.round((1-p.clip[1])*0.5*rast.height) ],
      qi = [ Math.round((1+q.clip[0])*0.5*rast.width), Math.round((1-q.clip[1])*0.5*rast.height) ],
      ri = [ Math.round((1+r.clip[0])*0.5*rast.width), Math.round((1-r.clip[1])*0.5*rast.height) ];
  
  //arrange vertices so pi.y <= qi.y <= ri.y
  if(pi[1] > ri[1]){ var temp = pi; pi = ri; ri = temp; }
  if(pi[1] > qi[1]){ var temp = pi; pi = qi; qi = temp; }
  if(qi[1] > ri[1]){ var temp = ri; ri = qi; qi = temp; }
  
  //clamp limiting y-values
  var y0 = pi[1], y1 = ri[1]; 
  if(y0 < 0){ y0 = 0; }else if(y0 >= rast.height){ return; }
  if(y1 < 0){ return; }else if(y1 >= rast.height){ y1 = rast.height-1; }
  
  //slopes (reciprocal) of rasterized edges (horizontal cases are handled appropriately)
  var mpq = (qi[0] - pi[0]) / (qi[1] - pi[1]),
      mqr = (ri[0] - qi[0]) / (ri[1] - qi[1]),
      mpr = (ri[0] - pi[0]) / (ri[1] - pi[1]);
  //-------------------------------------------
  
  //used to convert eye coords -> barycentric coords
  Vector3d.setAsCross(qxr, q.eye, r.eye);
  Vector3d.setAsCross(rxp, r.eye, p.eye);
  Vector3d.setAsCross(pxq, p.eye, q.eye);
  
  //function for a single scanline: x-values need not be rounded or sorted
  var scan = function(x0, x1){
  
    //round, sort, and clamp x
    if(x0 > x1){ var temp = x0; x0 = x1; x1 = temp; }
    if(x0 < 0){ x0 = 0; }else if(x0 >= rast.width){ return; }
    if(x1 < 0){ return; }else if(x1 >= rast.width){ x1 = rast.width-1; }
    x0 = x0 | 0; //Math.floor(x0);
    x1 = x1 | 0; //Math.floor(x1);
    
    //start computing barycentric coords (account for x term below)
    var vy = y*rast.yScrToEye + rast.yScrToEye0, //y component, in eye coords, of pixels's ray with z = -1
        lp0 = vy*qxr[1] - qxr[2],
        lq0 = vy*rxp[1] - rxp[2],
        lr0 = vy*pxq[1] - pxq[2];
    
    //horizontal scan
    rast.setPosition(x0,y);
    for(var x = x0; x <= x1; x++){
    
      //finish computing barycentric coords
      var vx = x*rast.xScrToEye + rast.xScrToEye0, //x component, in eye coords, of pixels's ray with z = -1
          lp = vx*qxr[0] + lp0, // = [vx,vy,-1].qxr
          lq = vx*rxp[0] + lq0, // = [vx,vy,-1].rxp
          lr = vx*pxq[0] + lr0; // = [vx,vy,-1].pxq
      
      //adjust to fix some glitches when accuracy is bad
      if(lp+lq+lr < 0){ if(lp > 0){ lp = 0; } if(lq > 0){ lq = 0; } if(lr > 0){ lr = 0; } }
      else{ if(lp < 0){ lp = 0; } if(lq < 0){ lq = 0; } if(lr < 0){ lr = 0; } }
      
      var suminv = 1.0 / (lp+lq+lr); 
      lp *= suminv; lq *= suminv; lr *= suminv;
      
      //clipping and zbuffer
      var zEye = lp*p.eye[2] + lq*q.eye[2] + lr*r.eye[2];
      
      var skip = false;
      if( (-zEye > rast.zFar) || (-zEye < rast.zNear) ){ skip = true; }
      else if(rast.zbuffer[rast.zbufferIndex] > zEye){ skip = true; }
      
      if(skip){ 
        rast.bufferIndex+=4; 
        rast.zbufferIndex++;
        continue;
      }
      
      //pass barycentric coords to "frag shader" to get color; draw pixel
      fragShader(pColor, lp, lq, lr);
      rast.setPixel(pColor, zEye);
    }
  };
  //-------------------------------------------
  
  //this loop only iterates if py < qy <= ry; i.e., it won't happen if pq or pr is horizontal
  for(var y=y0; y<qi[1]; y++){
    scan(pi[0] + mpr*(y - pi[1]), pi[0] + mpq*(y - pi[1]));
  }
  //if either pr or qr is horizontal, only one scanline left
  if(pi[1] == ri[1]){ scan(pi[0], ri[0]); }
  else if(qi[1] == ri[1]){ scan(qi[0], ri[0]); }
  //otherwise loop through remaining scanlines
  else{
    for(var y=qi[1]; y<=y1; y++){
      scan(pi[0] + mpr*(y - pi[1]), qi[0] + mqr*(y - qi[1]));
    }
  }
};

