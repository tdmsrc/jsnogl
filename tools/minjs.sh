#!/bin/sh

#run from /tools folder; parent contains /src

cd ../src

echo "Assembling OBJ resource(s)"
nodejs ../tools/staticrc.js \
twisted_pillar.obj obj/twisted_pillar.obj \
-o obj/objrc.js

echo "Minifying JS: jsnogl.min.js"
$(npm bin)/uglifyjs \
js/jsnoglMath.js \
js/jsnoglRasterizer.js \
js/jsnoglRaytraceDensity.js \
js/jsnoglMesh.js \
js/jsnoglMeshObj.js \
js/jsnoglMeshGraph.js \
js/jsnoglMeshContour.js \
js/jsnoglCoordinateBox.js \
js/jsnoglTexture.js \
-m -o jsmin/jsnogl.min.js

echo "Minifying JS: main.min.js"
$(npm bin)/uglifyjs \
obj/objrc.js \
js/mousedrag.js \
js/main.js \
-m -o jsmin/main.min.js
