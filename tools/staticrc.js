var fs = require('fs');

var GLOBAL_RC_NAME = "StaticTextRC";
var ENCODING_WRITE = "utf8";
var ENCODING_READ = "utf8";


//read command line arguments
//-------------------------------------------------
var instructions = "Invalid arguments: Should be [key0] [file0] [key1] [file1] ... -o [fileOut]";

//get file out
var oindex = process.argv.indexOf("-o");
if(oindex < 0){
  console.log(instructions);
  process.exit();
}

var outputFile = process.argv[oindex+1];

//get list of keys and file locations
var args = process.argv.slice(2,oindex).concat(process.argv.slice(oindex+2));
if(args.length % 2){ 
  console.log(instructions);
  process.exit();
}

var inputKeys = [], inputFiles = [];
for(var i=0; i<args.length/2; i++){
  inputKeys[i] = args[2*i];
  inputFiles[i] = args[2*i+1];
}


//tools for constructing JS string literal from actual string
//-------------------------------------------------
//escape backslashes and quotes so that the string can be output as a JS string literal
var escapeLine = function(line){
  line = line.replace(/\\/g, "\\\\");
  line = line.replace(/\"/g, "\\\"");
  line = line.replace(/\'/g, "\\\'");
  
  return line;
};

//assemble JS string literal which, when evaluated, will equal raw
var makeLiteral = function(raw){
  var lines = raw.match(/^(.*)$/mg);
  
  var literal = "\"";
  for(var i=0; i<lines.length; i++){
    var esc = escapeLine(lines[i]);
    if(i > 0){ literal += "\\n"; }
    literal += esc;
  }
  literal += "\"";
  
  return literal;
};


//tools for constructing complete JS file from resource data
//-------------------------------------------------
var writeOutputFile = function(data){
  fs.writeFile(outputFile, data, ENCODING_WRITE, function(err){
    if(err){
      console.log("[" + outputFile + "] Failed: " + err);
      return;
    }
    console.log("[" + outputFile + "] Successfully written!");
  }); 
};

var filesPending = inputFiles.length;
var jsoutBody = [];

var addDoneAlways = function(){
  filesPending--;
  
  if(filesPending <= 0){
    var jsout = "(function(_RC, undefined){\n";
    for(var i=0; i<jsoutBody.length; i++){
      jsout += jsoutBody[i];
    }
    jsout += "})(window." + GLOBAL_RC_NAME + " = window." + GLOBAL_RC_NAME + " || {});";
    
    writeOutputFile(jsout);
  }
};

var addDoneSuccess = function(resourceKey, literal){
  jsoutBody.push("  _RC[\"" + resourceKey + "\"] = " + literal + ";\n");
  addDoneAlways();
};


//add a definition to the JS body
var addDef = function(resourceKey, fileLocation){
  console.log("[" + resourceKey + "] Loading \"" + fileLocation + "\"...");
  
  fs.readFile(fileLocation, ENCODING_READ, function(err, data){
    //skip with message if readFile fails
    if(err){ 
      console.log("[" + resourceKey + "] Failed: " + err);
      addDoneAlways();
      return;
    }
    
    //construct JS string literal
    var literal = makeLiteral(data);
    
    //verify that strings will match
    var interp = "";
    eval("interp = " + literal + ";");
    var matched = (interp == data);
    
    if(matched){
      console.log("[" + resourceKey + "] Verified match!");
    }else{
      console.log("[" + resourceKey + "] Warning: match failed");
    }
    
    //add line to JS body
    addDoneSuccess(resourceKey, literal);
  });
};


//execute
//-------------------------------------------------
for(var i=0; i<inputFiles.length; i++){
  addDef(inputKeys[i], inputFiles[i]);
}

